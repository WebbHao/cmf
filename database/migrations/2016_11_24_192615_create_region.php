<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //大洲
        Schema::create('continents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->string('pin_yin');
            $table->string('first_letter');
            $table->string('short');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->integer('author_id')->default('1')->comment('作者编号');
            $table->timestamps();
        });

        //国家
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('continent_id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->string('pin_yin');
            $table->string('first_letter');
            $table->string('short');
            $table->string('national_flag')->comment('国旗图标');
            $table->string('country_code')->comment('二字码');
            $table->string('three_code')->comment('三字码');
            $table->enum('is_hot',['1','0'])->default('0')->comment('是否热门(1热门，0 不是)');
            $table->text('description')->comment('描述');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->string('longitude')->default('')->comment('经度');
            $table->string('latitude')->default('')->comment('纬度');
            $table->integer('author_id')->default('1')->comment('作者编号');
            $table->timestamps();
        });

        //城市
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->string('pin_yin');
            $table->string('first_letter');
            $table->string('short');
            $table->string('national_flag')->comment('国旗图标');
            $table->string('country_code')->comment('二字码');
            $table->string('three_code')->comment('三字码');
            $table->enum('is_hot',['1','0'])->default('0')->comment('是否热门(1热门，0 不是)');
            $table->integer('region_id')->comment('区域编号');
            $table->text('description')->comment('描述');
            $table->string('longitude')->default('')->comment('经度');
            $table->string('latitude')->default('')->comment('纬度');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->integer('author_id')->default('1')->comment('作者编号');
            $table->timestamps();
        });

        //地区
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->integer('enum_id')->default('0')->comment('分类');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->integer('author_id')->default('1')->comment('作者编号');
            $table->timestamps();
        });

        //标签
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->integer('enum_id')->comment('tag类型编号');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->integer('author_id')->default('1')->comment('作者编号');
            $table->timestamps();
        });

        //分类
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->integer('parent_id')->default('0')->comment('父类编号');
            $table->integer('author_id')->default('1')->comment('作者编号');
            $table->timestamps();
        });

        //POI
        Schema::create('pois', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->string('pin_yin');
            $table->integer('continent_id');
            $table->integer('country_id');
            $table->integer('region_id');
            $table->integer('city_id');
            $table->text('street')->comment('街道');
            $table->text('address')->comment('详细地址');
            $table->text('map_url')->comment('地图地址');
            $table->integer('one_type_id')->comment('一级分类');
            $table->integer('two_type_id')->comment('二级分类');
            $table->enum('recommend',['1','0'])->comment('推荐（1推荐，0不推荐）');
            $table->text('overview')->comment('');
            $table->text('tips')->comment('温馨提示');
            $table->text('content')->comment('内容正文');
            $table->string('longitude')->default('')->comment('经度');
            $table->string('latitude')->default('')->comment('纬度');
            $table->enum('is_hot',['1','0'])->default('0')->comment('是否热门(1热门，0 不是)');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->integer('author_id')->default('1')->comment('作者编号');
            $table->bigInteger('pv')->default('0')->comment('pv');
            $table->timestamps();
        });

        //评论
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('email');
            $table->integer('poi_id');
            $table->enum('recomend',['1','0'])->comment('推荐（1置顶，0不置顶）');
            $table->text('content')->comment('内容正文');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->integer('author_id')->default('0')->comment('作者编号');
            $table->timestamps();
        });

        //全类型分类,不分级
        Schema::create('enums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enum_id')->comment("全类型分类编号");
            $table->string('enum_name_cn')->comment('全类型分类名称中文');
            $table->string('enum_name_en')->comment('全类型分类名称英文');
            $table->enum('status',['1','0'])->default('1')->comment('状态(1启用,0禁用)');
            $table->string('category')->default('')->comment('全类型');
            $table->integer('author_id')->default('0')->comment('作者编号');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('continent');
        Schema::drop('country');
        Schema::drop('city');
        Schema::drop('region');
        Schema::drop('tag');
        Schema::drop('type');
        Schema::drop('pois');
        Schema::drop('comment');
        Schema::drop('enums');
    }
}
