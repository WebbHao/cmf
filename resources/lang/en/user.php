<?php 
return [
	'login'    => 'Sign In',
	'logout'   => 'Sign Out',
	'register' => 'Register',
	'profile'  => 'Profile',
];