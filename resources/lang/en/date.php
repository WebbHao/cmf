<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Date Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "january"   => 'January',
    "february"  => 'February',
    "march"     => 'March',
    "april"     => 'April',
    "may"       => 'May',
    "june"      => 'June',
    "july"      => 'July',
    "august"    => 'August',
    "september" => 'September',
    "october"   => 'October',
    "november"  => 'November',
    "december"  => 'December',

];
