<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码最少需要包含6个字符，并匹配确认',
    'reset'    => '您的密码已经重置!',
    'sent'     => '重置密码的链接已经发送到您的邮箱!',
    'token'    => '重置密码的令牌失效',
    'user'     => "该邮箱地址不存在",

];
