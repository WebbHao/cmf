<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ' :attribute 必须是许可的.',
    'active_url'           => ' :attribute 不是一个合法的URL.',
    'after'                => ' :attribute 必须在 :date 之后.',
    'alpha'                => ' :attribute 只允许输入字母.',
    'alpha_dash'           => ' :attribute 只允许输入字母、数字和破折号.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => ' :attribute 最小长度 :min.',
        'file'    => ' :attribute 最小 :min KB.',
        'string'  => ' :attribute 最少 :min 字符.',
        'array'   => ' :attribute 最少包含 :min 个元素.',
    ],
    'not_in'               => '当前选择 :attribute 不合法.',
    'numeric'              => ' :attribute 必须是数字.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => ' :attribute 格式不合法.',
    'required'             => ' :attribute 必填.',
    'required_if'          => ' :attribute 必填,当 :other 是 :value.',
    'required_unless'      => ' :attribute 必填,除非 :other 包含 :values.',
    'required_with'        => ' :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => ' :attribute 和 :other 必须匹配.',
    'size'                 => [
        'numeric' => ' :attribute 必须是 :size.',
        'file'    => ' :attribute 必须是 :size KB.',
        'string'  => ' :attribute 必须是 :size KB.',
        'array'   => ' :attribute 必须包含 :size 个元素.',
    ],
    'string'               => ' :attribute 必须是一个字符串.',
    'timezone'             => ' :attribute 必须是一个合法的区间.',
    'unique'               => ' :attribute 已经存在.',
    'url'                  => ' :attribute 格式错误.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => '自定义消息',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
