<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Date Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "january"   => '一月',
    "february"  => '二月',
    "march"     => '三月',
    "april"     => '四月',
    "may"       => '五月',
    "june"      =>'六月',
    "july"      => '七月',
    "august"    => '八月',
    "september" =>'九月',
    "october"   => '十月',
    "november"  => '十一月',
    "december"  => '十二月',

];
