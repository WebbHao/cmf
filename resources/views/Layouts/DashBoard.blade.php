@extends('Layouts.Plain')

@section('body')
	<div id="app-container">
		<nav class="navbar navbar-inverse navbar-fixed-top striped-bg" id="top-navbar">
		  <div class="container-fluid">
		    <div class="navbar-header">	      
		      <a class="sidenav-toggle" href="#"><span class="brandbar"><i class="fa fa-bars hidd"></i></span></a>
		      <a class="navbar-brand" href="{{ url ('') }}"><i class="fa fa-paper-plane"></i>&nbsp;CMF</a> <div class="solution">>&nbsp;</i>内容管理开发框架</div>
		    </div>
			<div class="right-admin">
				<ul>
					<li class="dropdown hidd">
			          	<a href="#" class="dropdown-toggle admin-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
			          		<img class="img-circle admin-img" src="{{ url ('img/profile1.jpg') }}" alt="">
						</a>
			          	 <ul class="dropdown-menu admin" role="menu">
				          	<li role="presentation" class="dropdown-header">Admin name</li>
				            <li><a href="{{ url ('profile') }}"><i class="fa fa-info"></i> Profile</a></li>
				            <li><a href="javascript:void(0);"><i class="fa fa-power-off"></i> Logout</a></li>
				        </ul>		          
			        </li>
		        </ul>
			</div>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav navbar-right top-nav">
		      	
		      	<li class="dropdown">
		          	<a href="#" class="dropdown-toggle notoggle" data-toggle="dropdown" role="button" aria-expanded="false">
		          		<span><i class="fa fa-circle text-primary"></i></span>
		          	</a>
		          	<ul class="dropdown-menu" role="menu">
			            <li class="padder-h-xs">
			            	<table class="table color-swatches-table text-center no-m-b">
			            		<tr>
			            			<td>
			            				<a href="javascript:;" data-theme="green" class="theme-picker">
			            					<i class="fa fa-circle fa-2x green-base"></i>
			            				</a>
			            			</td>
			            			<td>
			            				<a href="javascript:;" data-theme="blue" class="theme-picker">
			            					<i class="fa fa-circle fa-2x blue-base"></i>
			            				</a>
			            			</td>
			            		</tr>
			            		<tr>
			            			<td>
			            				<a href="javascript:;" data-theme="red" class="theme-picker">
			            					<i class="fa fa-circle fa-2x red-base"></i>
			            				</a>
			            			</td>
			            			<td>
			            				<a href="javascript:;" data-theme="orange" class="theme-picker">
			            					<i class="fa fa-circle fa-2x orange-base"></i>
			            				</a>
			            			</td>
			            		</tr>
			            	</table>
			            </li>
		          	</ul>
		        </li>
		        <li>
		      		<a href="javascript:;" id="boxed-layout"><i class="fa fa-expand-toggle fa-tp"></i></a>
		      	</li>
		      	<li class="dropdown">
		          	<a href="#" class="dropdown-toggle notoggle" data-toggle="dropdown" role="button" aria-expanded="false">
		          		<span class="notification"><i class="fa fa-bell-o"></i></span>
		        		<b class="badge nobadge">5</b>
		          	</a>
		          	<ul class="dropdown-menu dropdown-menu-lg ddown" role="menu">
			          	<li role="presentation" class="dropdown-header">Notification</li>
			            <li>
			            	<a href="#" class="notification-wrap">
			            		<div class="notification-media">
			            			<span class="fa-stack fa-lg">
								  		<i class="fa fa-circle fa-stack-2x text-warning"></i>
								  		<i class="fa fa-user fa-stack-1x fa-inverse"></i>
									</span>
									<div><span class="label label-danger">Urgent</span></div>
								</div>
								<div class="notification-info">
									<div class="time-info text-muted pull-right"><small><i class="fa fa-comments"></i> 2 hours ago</small></div>
									<h5>Heading </h5>
									<p>Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...</p>
								</div>			            		
			            	</a>
			            </li>
			            <li class="divider"></li>
			            <li>
			            	<a href="#" class="notification-wrap">
			            		<div class="pull-left notification-media">
			            			<span class="fa-stack fa-lg">
								  		<i class="fa fa-circle fa-stack-2x text-primary"></i>
								  		<i class="fa fa-user fa-stack-1x fa-inverse"></i>
									</span>
									<div><span class="label label-info">New</span></div>
								</div>
								<div class="notification-info">
									<div class="time-info text-muted pull-right" ><small><i class="fa fa-comments"></i> 23rd Dec 2014</small></div>
									<h5>Heading </h5>
									<p>Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...</p>
								</div>			            		
			            	</a>
			            </li>
		          	</ul>
		        </li>
		        <li class="dropdown">
		          	<a href="#" class="dropdown-toggle admin-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		          		<img class="img-circle admin-img" src="{{ url ('img/profile1.jpg') }}" alt="">&nbsp;&nbsp;&nbsp;<span class="add">{{Auth::check()?Auth::user()->name:'--' }}&nbsp;
						<i class="fa fa-angle-down"></i></span>
		          	</a>
		          	<ul class="dropdown-menu admin" role="menu">
						<li role="presentation" class="dropdown-header">{{Auth::check()?Auth::user()->name:'--' }}</li>
						<li>
							<a href="{{ url ('profile') }}">
								<i class="fa fa-info"></i>
								{{trans('user.profile')}}
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<i class="fa fa-power-off"></i>
								{{trans('user.logout')}}
							</a>
						</li>
			        </ul>		          
		        </li>
		       </ul>
		    </div>
		</div>
		</nav>
		@include('Widgets.Menus.Composer')

		<div id="body-container">
			@if (trim($__env->yieldContent('pageHeading')))
    		<div class="page-title clearfix">
    			<div class="pull-left">
    				<h1>@yield('pageHeading')</h1>
    				<small class="subtitle">@yield ('pageSubTitle')</small>
    			</div>
    			<ol class="breadcrumb pull-right">
			 		<li class="active">@yield('pageHeading')</li>
			  		<li><a href="{{ url ('/home') }}"><i class="fa fa-tachometer"></i></a></li>
			 	</ol>
			</div>
    		@endif
			<!-- <div class="statswrap">
				@yield ('stats')
			</div> -->
			<div class="conter-wrapper">				
				@yield('DashBoard-Content')
			</div>

			<div id="footer-wrap" class="footer">
				Copyright © 2017 Dashy Theme
				<span class="pull-right">
					<a href="javascript:;"><i class="fa fa-facebook-square"></i></a>
					<a href="javascript:;">&nbsp;<i class="fa fa-twitter-square"></i></a>
				</span>
			</div>
		</div>
	</div>

@stop

@section('js')

@parent

<script type="text/javascript">

	$(function(){

		// Sidebar Charts

		// Pie Chart
		var chart3 = c3.generate({
		   bindto: '#sidebar-piechart',
		    data: {

		        // iris data from R
		        columns: [
		            ['1', 36],
		            ['2', 54],
		            ['3', 12],
		        ],
		        type : 'pie',
		        onclick: function (d, i) { console.log("onclick", d, i); },
		        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
		        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
		    },
		    color: {
		        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
		    },
		    pie: {
		      expand: true
		    },
		    size: {
		      width: 140,
		      height: 140
		    },
		    tooltip: {
		      show: false
		    }

		});



		// Bar Chart
		var chart6 = c3.generate({
		    bindto: '#sidebar-barchart',
		    data: {
		        columns: [
		            ['data1', 30, 200, 100, 400, 250, 310, 90, 125, 50]
		        ],
		        type: 'bar'
		    },
		    bar: {
		        width: {
		            ratio: 0.8
		        }
		    },
		    size: {
		      width: 200,
		      height: 120
		    },
		    tooltip: {
		      show: false
		    },
		    color: {
		        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
		    },
		    axis: {
		      y: {
		        show: false,
		        color: '#ffffff'
		      }
		}
		});


		// Sidebar Tabs
		$('#navTabs .sidebar-top-nav a').click(function (e) {
		 	e.preventDefault()
		 	$(this).tab('show');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 10);

		});



		$('.subnav-toggle').click(function() {
			$(this).parent('.sidenav-dropdown').toggleClass('show-subnav');
			$(this).find('.fa-angle-down').toggleClass('fa-flip-vertical');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 500);

		});

	    $('.sidenav-toggle').click(function() {
	        $('#app-container').toggleClass('push-right');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 500);

	    });


	    // Boxed Layout Toggle
		$('#boxed-layout').click(function() {

	        $('body').toggleClass('box-section');

	        var hasClass = $('body').hasClass('box-section');

	        $.get('/api/change-layout?layout='+ (hasClass ? 'boxed': 'fluid'));

		});



		$('.tab-content-scroller').perfectScrollbar();

		$('.theme-picker').click(function() {
			changeTheme($(this).attr('data-theme'));
		});


	});

	function changeTheme(theme) {

		$('<link>')
		  .appendTo('head')
		  .attr({type : 'text/css', rel : 'stylesheet'})
		  .attr('href', '/css/app-'+theme+'.css');

			$.get('/api/change-theme?theme='+theme);
	}


</script>
<script type="text/javascript">
	$.ajaxSetup( {
		headers: { // 默认添加请求头
			"X-CSRF-TOKEN": "{{csrf_token()}}"
		} ,
		//error: function(jqXHR, textStatus, errorMsg){ // 出错时默认的处理函数
		// jqXHR 是经过jQuery封装的XMLHttpRequest对象
		// textStatus 可能为： null、"timeout"、"error"、"abort"或"parsererror"
		// errorMsg 可能为： "Not Found"、"Internal Server Error"等

		// 提示形如：发送AJAX请求到"/index.html"时出错[404]：Not Found
		//    alert( '发送AJAX请求到"' + this.url + '"时出错[' + jqXHR.status + ']：' + errorMsg );
		//}
	} );

</script>
@stop