@extends ('Layouts.plain')
@section("left_content")
<div class="back">
	<div class="errors col-sm-8 col-sm-offset-2">
		@yield ('errors')
	</div>

	<div class="login-outer">
		<div class="login-wrap">
			<div class="login-left striped-bg text-center">
				<a href="{{ url('/') }}" class="logo-lg">
					<i class="fa fa-paper-plane"></i> CMF
				</a>
				<div class="slogan">内容管理开发框架</div>
			</div>
			
			<div class="login-right striped-bg">
				@yield('page-content')
			</div>
		</div>
	</div>
</div>
@show
