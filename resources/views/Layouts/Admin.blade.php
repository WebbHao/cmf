<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<!-- Mirrored from dashy.strapui.com/laravel/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Aug 2015 11:40:16 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8"/>
	<title>{{$title or 'Welcome'}} | {{Config::get('app.name')}}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta content="{{$keywords or ''}}" name="keywords"/>
	<meta content="{{$description or ''}}" name="description"/>
	<meta content="{{Config::get('app.author')}}" name="author"/>
	@section('Css')
	@include('Layouts.Module.CommonCss')
	@show
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="loader-block">
	<span class="loader-inner line-scale">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</span>
</div>
<div id="app-container">
	@section('Header')
		@include('Layouts.Module.Header')
	@show
	@section('Sider')
		@include('Layouts.Module.Sider')
	@show

	<div id="{{$htmlContentId or 'body-container'}}">
		@yield('content')
		@section('footer')
			@include('Layouts.Module.footer')
		@show
	</div>	
</div>
@section('Js')
@include('Layouts.Module.CommonJs');
@show
</body>

<!-- Mirrored from dashy.strapui.com/laravel/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Aug 2015 11:40:53 GMT -->
</html>