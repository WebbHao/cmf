<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>

	<meta charset="utf-8"/>

	<title>{{$title or 'CMF'}} | TrueInfo</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta content="{{$description or 'TrueInfo CMF'}}" name="description"/>
	<meta content="{{$keywords or 'TrueInfo CMF'}}" name="keywords"/>
	<meta content="{{$author or 'Justin.W<justin.bj@ms.com>'}}" name="author"/>
	<meta content="{{ csrf_token() }}" name="csrf-token" />

	<link rel="stylesheet" href="{{ asset("css/vendor.css") }}" />
	<link rel="stylesheet" href="{{ asset("css/app-".(\Session::has('theme') ? \Session::get('theme') : 'green').".css") }}" />
	{{-- <link rel="stylesheet" href="{{ asset("css/select2.css") }}" /> --}}
	@yield('css')
</head>
<body class="page-header-fixed page-quick-sidebar-over-content {{ \Session::get('layout') == 'boxed' ? 'box-section' : '' }}">
	<div class="loader-block">
		<span class="loader-inner line-scale">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</span>
	</div>
	@yield('body')

	<script src="{{asset('static/js/jquery-3.1.1.js')}}" type="text/javascript"></script>
	{{-- <script src="{{asset('static/js/jquery-min-1.9.0.js')}}" type="text/javascript"></script>
     --}}
	<script src="{{ asset("js/vendor.js") }}" type="text/javascript"></script>
	{{--<script src="{{ asset("vendor/ckeditor/ckeditor.js") }}" type="text/javascript"></script>--}}
	<script src="{{ asset("js/app.js") }}" type="text/javascript"></script>
	{{-- <script src="{{ asset("js/select2.full.min.js") }}" type="text/javascript"></script> --}}
	<script src="{{asset('js/common.js')}}" type="text/javascript"></script>
	@yield('js')

</body>
</html>