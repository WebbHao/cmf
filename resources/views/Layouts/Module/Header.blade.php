<nav class="navbar navbar-inverse navbar-fixed-top striped-bg" id="top-navbar">
	<div id="app-container">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="sidenav-toggle" href="#">
					<span class="brandbar"> <i class="fa fa-bars hidd"></i>
					</a>
				</span>
				<a class="navbar-brand" href="index.html"> <i class="fa fa-paper-plane"></i>
					&nbsp;Dashy
				</a>
				<div class="solution">>&nbsp;</i>
				By the Developers. For the Developers
			</div>
		</div>
		<div class="right-admin">
			<ul>
				<li class="dropdown hidd">
					<a href="#" class="dropdown-toggle admin-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<img class="img-circle admin-img" src="{{asset('static/img/profile1.jpg')}}" alt=""></a>
					<ul class="dropdown-menu admin" role="menu">
						<li role="presentation" class="dropdown-header">{{Auth::user()->name }}</li>
						<li>
							<a href="profile.html">
								<i class="fa fa-info"></i>
								{{trans('user.profile')}}
							</a>
						</li>
						<li>
							<a href="{{ url('/logout') }}">
								<i class="fa fa-power-off"></i>
								{{trans('user.logout')}}
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right top-nav">
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle notoggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<span>
							<i class="fa fa-circle text-primary"></i>
						</span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li class="padder-h-xs">
							<table class="table color-swatches-table text-center no-m-b">
								<tr>
									<td>
										<a href="javascript:;" data-theme="green" class="theme-picker">
											<i class="fa fa-circle fa-2x green-base"></i>
										</a>
									</td>
									<td>
										<a href="javascript:;" data-theme="blue" class="theme-picker">
											<i class="fa fa-circle fa-2x blue-base"></i>
										</a>
									</td>
								</tr>
								<tr>
									<td>
										<a href="javascript:;" data-theme="red" class="theme-picker">
											<i class="fa fa-circle fa-2x red-base"></i>
										</a>
									</td>
									<td>
										<a href="javascript:;" data-theme="orange" class="theme-picker">
											<i class="fa fa-circle fa-2x orange-base"></i>
										</a>
									</td>
								</tr>
							</table>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;" id="boxed-layout">
						<i class="fa fa-expand-toggle fa-tp"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle notoggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<span class="notification">
							<i class="fa fa-bell-o"></i>
						</span> <b class="badge nobadge">5</b>
					</a>
					<ul class="dropdown-menu dropdown-menu-lg ddown" role="menu">
						<li role="presentation" class="dropdown-header">Notification</li>
						<li>
							<a href="#" class="notification-wrap">
								<div class="notification-media">
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x text-warning"></i>
										<i class="fa fa-user fa-stack-1x fa-inverse"></i>
									</span>
									<div>
										<span class="label label-danger">Urgent</span>
									</div>
								</div>
								<div class="notification-info">
									<div class="time-info text-muted pull-right">
										<small>
											<i class="fa fa-comments"></i>
											2 hours ago
										</small>
									</div>
									<h5>Heading</h5>
									<p>
										Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...
									</p>
								</div>
							</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#" class="notification-wrap">
								<div class="pull-left notification-media">
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x text-primary"></i>
										<i class="fa fa-user fa-stack-1x fa-inverse"></i>
									</span>
									<div>
										<span class="label label-info">New</span>
									</div>
								</div>
								<div class="notification-info">
									<div class="time-info text-muted pull-right">
										<small>
											<i class="fa fa-comments"></i>
											23rd Dec 2014
										</small>
									</div>
									<h5>Heading</h5>
									<p>
										Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...
									</p>
								</div>
							</a>
						</li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle admin-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<img class="img-circle admin-img" src="{{asset('static/img/profile1.jpg')}}" alt="">
						&nbsp;&nbsp;&nbsp;
						<span class="add">
							{{Auth::user()->name }}&nbsp;
							<i class="fa fa-angle-down"></i>
						</span>
					</a>
					<ul class="dropdown-menu admin" role="menu">
						<li role="presentation" class="dropdown-header">{{Auth::user()->name }}</li>
						<li>
							<a href="profile.html">
								<i class="fa fa-info"></i>
								{{trans('user.profile')}}
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<i class="fa fa-power-off"></i>
								{{trans('user.logout')}}
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
</nav>