<div class="striped-bg" id="sidenav">
    <div role="tabpanel" id="navTabs">
        <div class="sidebar-controllers">
            <ul class="nav nav-tabs nav-justified sidebar-top-nav" role="tablist">
                <li role="presentation" class="active"><a href="#menu"><i class="fa fa-bars"></i></a></li>
                <li role="presentation"><a href="#comments"><i class="fa fa-comments-o"></i></a></li>
                <li role="presentation"><a href="#charts"><i class="fa fa-line-chart"></i></a></li>
                <li role="presentation"><a href="#calendar"><i class="fa fa-calendar"></i></a></li>
                <li role="presentation"><a href="#notification"><i class="fa fa-bell-o"></i></a></li>
            </ul>                   
            <div class="">
                <div class="tab-content-scroller tab-content sidebar-section-wrap">
                    <div role="tabpanel" class="tab-pane active" id="menu">
                        <div class="photo-container text-center">
                            <a href="{{ url ('profile') }}">
                                <img src="{{ url ('img/profile1.jpg') }}" alt="" class="img-circle dash-profile" />
                            </a>
                            <div class="t-p">
                                <a href="{{ url ('profile') }}">Justin.W</a>
                            </div>
                        </div>
                        <div class="section-heading">Menus</div>
                        <ul class="nav sidebar-nav ">
                           
                            @if(is_array($menus)&&count($menus))
                            @foreach($menus as $key => $val)
                                @if(array_has($val, 'sub_list')&&array_get($val,'sub_list'))
                                    <li class="sidenav-dropdown {{ (Request::is('*'.array_get($val,'match_url','/').'*') ? 'show-subnav' : '') }}">
                                        <a class="subnav-toggle" href="javascript:;">
                                            <i class="{{array_get($val,'icon','fa fa-circle-o')}}"></i> {{array_get($val,'name')}}
                                            <i class="fa fa-angle-down {{ (Request::is('*'.array_get($val,'match_url','/').'*') ? 'fa-flip-vertical' : '') }} pull-right"></i>
                                        </a>
                                        <ul class="nav sidenav-sub-menu">
                                            @foreach($val['sub_list'] as $k => $v)
                                            <li {{ (Request::is('*/'.array_get($v,'match_url','/').'*') ? 'class=active' : '') }}>
                                                @if(array_get($v,'control')&&array_get($v,'action'))
                                                <a href="{{action(array_get($v,'control').'@'.array_get($v,'action'),[array_get($v,'params')])}}">
                                                    <i class="{{array_get($v,'icon','fa fa-circle-o')}}"></i> {{array_get($v,'name')}}
                                                </a>
                                                @else
                                                <a href="{{url('')}}">
                                                    <i class="fa fa-circle-o"></i> {{array_get($v,'name')}}
                                                </a>
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li {{ (Request::is('*'.array_get($val,'match_url')) ? 'class=active' : '') }} >
                                    @if(array_get($val,'control')&&array_get($val,'action'))
                                    <a href="{{action(array_get($val,'control').'@'.array_get($val,'action'),[array_get($val,'params')])}}">
                                        <i class="{{array_get($val,'icon','fa fa-circle-o')}}"></i> {{array_get($val,'name')}}
                                    </a>
                                    @else
                                    <a href="{{url('')}}">
                                        <i class="{{array_get($val,'icon','fa fa-circle-o')}}"></i> {{array_get($val,'name')}}
                                    </a>
                                    @endif
                                    </li>
                                @endif
                            @endforeach
                            @endif
                            @if(Auth::user()->is_supper)
                            <li {{ (Request::is('home') ? 'class=active' : '') }} ><a href="{{ url ('home') }}"><i class="fa fa-home"></i> Home</a>
                            </li>
                            <li class="sidenav-dropdown {{ (Request::is('*ui-element/*') ? 'show-subnav' : '') }}">
                                <a class="subnav-toggle" href="javascript:;"><i class="fa fa-toggle-off"></i> UI Elements <i class="fa fa-angle-down {{ (Request::is('*ui-element/*') ? 'fa-flip-vertical' : '') }} pull-right"></i></a>
                                <ul class="nav sidenav-sub-menu">
                                    <li {{ (Request::is('*buttons') ? 'class=active' : '') }}><a href="{{ url ('ui-element/buttons') }}"><i class="fa fa-circle-o"></i> Button</a></li>
                                    <li {{ (Request::is('*dropdown') ? 'class=active' : '') }}><a href="{{ url ('ui-element/dropdown') }}"><i class="fa fa-arrows-v"></i> Dropdown</a></li>
                                    <li {{ (Request::is('*labels') ? 'class=active' : '') }}><a href="{{ url ('ui-element/other-elements') }}"><i class="fa fa-flag-o"></i> Other Elements</a></li>
                                    <li {{ (Request::is('*breadcrumbs') ? 'class=active' : '') }}><a href="{{ url ('ui-element/breadcrumbs') }}"><i class="fa fa-exchange"></i> Breadcrumbs & Pagination</a></li>
                                    <li {{ (Request::is('*progressbars') ? 'class=active' : '') }}><a href="{{ url ('ui-element/progressbars') }}"><i class="fa fa-angle-double-right"></i> Progressbars</a></li>
                                    <li {{ (Request::is('*alerts') ? 'class=active' : '') }}><a href="{{ url ('ui-element/alerts') }}"><i class="fa fa-warning"></i> Alerts</a></li>
                                    <li {{ (Request::is('*modal') ? 'class=active' : '') }}><a href="{{ url ('ui-element/modal') }}"><i class="fa fa-square-o"></i> Modal</a></li>
                                    <li {{ (Request::is('*collapse') ? 'class=active' : '') }}><a href="{{ url ('ui-element/collapse') }}"><i class="fa fa-align-justify"></i> Collapse</a></li>
                                    <li {{ (Request::is('*typography') ? 'class=active' : '') }}><a href="{{ url ('ui-element/typography') }}"><i class="fa fa-pencil"></i> Typography</a></li>
                                    <li {{ (Request::is('*icons') ? 'class=active' : '') }}><a href="{{ url ('ui-element/icons') }}"><i class="fa fa-cc-visa"></i> Icons</a></li>                                   
                                </ul>
                            </li>
                            <li  {{ (Request::is('*panel') ? 'class=active' : '') }}><a href="{{ url ('panel') }}"><i class="fa fa-bookmark-o"></i> Panel</a></li>
                            <li {{ (Request::is('*table') ? 'class=active' : '') }}><a href="{{ url ('table') }}"><i class="fa fa-table"></i> Table</a></li>
                            <li {{ (Request::is('*grid') ? 'class=active' : '') }}><a href="{{ url ('grid') }}"><i class="fa fa-th-large"></i> Grid</a></li>
                            <li class="sidenav-dropdown {{ (Request::is('*messages/*') ? 'show-subnav' : '') }}">
                                <a class="subnav-toggle" href="#"><i class="fa fa-pencil"></i> Mail <i class="fa fa-angle-down fa-angle-down {{ (Request::is('*messages/*') ? 'fa-flip-vertical' : '') }} pull-right"></i></a>
                                <ul class="nav sidenav-sub-menu">
                                    <li {{ (Request::is('*inbox') ? 'class=active' : '') }}><a href="{{ url ('messages/inbox') }}"><i class="fa fa-inbox"></i> Inbox</a></li>
                                    <li {{ (Request::is('*compose') ? 'class=active' : '') }}><a href="{{ url ('messages/compose') }}"><i class="fa fa-pencil-square-o"></i> Compose</a></li>
                                    
                                </ul>
                            </li>
                            <li class="sidenav-dropdown {{ (Request::is('*form/*') ? 'show-subnav' : '') }}">
                                <a class="subnav-toggle" href="#"><i class="fa fa-file-text-o"></i> Forms <i class="fa fa-angle-down fa-angle-down {{ (Request::is('*form/*') ? 'fa-flip-vertical' : '') }} pull-right"></i></a>
                                <ul class="nav sidenav-sub-menu">
                                    <li {{ (Request::is('*elements') ? 'class=active' : '') }}><a href="{{ url ('form/elements') }}"><i class="fa fa-edit"></i> Elememts</a></li>
                                    <li {{ (Request::is('*components') ? 'class=active' : '') }}><a href="{{ url ('form/components') }}"><i class="fa fa-keyboard-o"></i> Components</a></li>
                                </ul>
                            </li>
                            <li class="sidenav-dropdown {{ (Request::is('*chart/*') ? 'show-subnav' : '') }}">
                                <a class="subnav-toggle" href="#"><i class="fa fa-area-chart"></i> Charts <i class="fa fa-angle-down fa-angle-down {{ (Request::is('*chart/*') ? 'fa-flip-vertical' : '') }} pull-right"></i></a>
                                <ul class="nav sidenav-sub-menu">
                                    <li {{ (Request::is('*mcharts') ? 'class=active' : '') }}><a href="{{ url ('chart/chartjs') }}"><i class="fa fa-line-chart"></i> Chart.js</a></li>
                                    <li {{ (Request::is('*c3charts') ? 'class=active' : '') }}><a href="{{ url ('chart/c3charts') }}"><i class="fa fa-bar-chart"></i> C3.js</a></li>
                                </ul>
                            </li>
                            <li {{ (Request::is('*invoice') ? 'class=active' : '') }}>
                                <a href="{{ url ('messages/invoice') }}"><i class="fa fa-newspaper-o"></i> Invoice</a>
                            </li>
                            <li {{ (Request::is('*calendar') ? 'class=active' : '') }} ><a href="{{ url ('calendar') }}"><i class="fa fa-calendar"></i> Calendar</a>
                            </li>
                            <li><a href="{{ url ('/auth/login') }}"><i class="fa fa-sign-in"></i> Log In</a>
                            </li>
                            <li><a href="{{ url ('/auth/register') }}"><i class="fa fa-user-plus"></i> Sign Up</a>
                            </li>
                            <li><a href="{{ url ('404') }}"><i class="fa fa-exclamation-circle"></i> 404 Page</a>
                            </li>
                            <li><a href="{{ url ('blank') }}"><i class="fa fa-file-o"></i> Blank Page</a>
                            </li>
                            <li><a href="{{ url ('docs') }}"><i class="fa fa-file-o"></i> Docs</a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="comments">
                        <div class="section-heading">Members</div>
                        <ul class="online-members">
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/profile1.jpg') }}" alt="">Kumar Sanket <i class="fa fa-circle pull-right text-success"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Kumar Pratik <i class="fa fa-circle pull-right text-success"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Megha Kumari <i class="fa fa-circle pull-right text-success"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Sankhadeep Roy <i class="fa fa-circle pull-right text-success"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/profile.jpg') }}" alt="">Suraj Ahmad Choudhury <i class="fa fa-circle pull-right text-success"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/profile1.jpg') }}" alt="">Kumar Sanket <i class="fa fa-circle pull-right text-warning"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Megha Kumari <i class="fa fa-circle pull-right text-warning"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Kumar Pratik <i class="fa fa-circle pull-right text-warning"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Mrigank Mridul <i class="fa fa-circle pull-right text-muted"></i></a>
                            </li>
                            <li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Amith M S <i class="fa fa-circle pull-right text-muted"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="charts">
                        <div class="section-heading">Charts</div>
                        <div class="section-content text-center">
                            <h4>Today's View</h4>
                                <div class="chart-container">
                                    <div id="sidebar-piechart"></div>
                                </div>
                            <h4>Today's Signups</h4>
                                <div class="chart-container2">
                                    <div id="sidebar-barchart"></div>
                                </div>
                            <hr class="lighter">
                                <div class="transaction">
                                <h4 class="eft">Today's Signups</h4>
                                    @include('widgets.progress', array('class'=> '', 'value'=>'67.34%', 'badge'=>true))
                                    @include('widgets.progress', array('class'=> 'success', 'value'=>'87.95%', 'badge'=>true))
                                    @include('widgets.progress', array('class'=> 'warning', 'value'=>'27.64%', 'badge'=>true))
                                    @include('widgets.progress', array('class'=> 'danger', 'value'=>'12', 'badge'=>true))
                                </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="calendar">
                        <div class="section-heading">Today</div>
                            <ul class="today-ul">
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '7:00 AM'))</div><div class="happened">something happened</div>
                                    </a>
                                </li>
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '8:00 AM'))</div><div class="happened">something more happenned</div>
                                    </a>
                                </li>
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '9:00 AM'))</div><div class="happened">lorem ipsum happened</div>
                                    </a>
                                </li>
                            </ul>
                        <div class="section-heading">7th April 2015</div>
                            <ul class="today-ul dead">
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '7:00 AM'))</div><div class="happened">something happened</div>
                                    </a>
                                </li>
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '8:00 AM'))</div><div class="happened">something more happenned</div>
                                    </a>
                                </li>
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '9:00 AM'))</div><div class="happened">lorem ipsum happened</div>
                                    </a>
                                </li>
                            </ul>
                        <div class="section-heading">9th April 2015</div>
                            <ul class="today-ul dead">
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '7:00 AM'))</div><div class="happened">something happened</div>
                                    </a>
                                </li>
                                <li>
                                    <a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '8:00 AM'))</div><div class="happened">something more happenned</div>
                                    </a>
                                </li>
                            </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="notification">
                        <div class="section-heading">Notifications</div>
                        <div class="notification-info">
                            <ul class="notif-ul">
                                <li>
                                    <a href="" class="notification-wrap">
                                        <div class="notification-media">
                                            <span class="fa-stack fa-lg">
                                                <i class="fa fa-circle fa-stack-2x text-warning"></i>
                                                <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                                            </span>
                                            <div><span class="label label-danger">Urgent</span></div>
                                        </div>
                                        <div class="notification-info">
                                            <div class="time-info"><small><i class="fa fa-comments"></i> 2 hours ago</small></div>
                                            <h5>Heading </h5>
                                            <p>Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...</p>
                                        </div>                              
                                    </a>
                                </li>
                                <li><a href="" class="notification-wrap">
                                    <div class="pull-left notification-media">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                            <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <div><span class="label label-info">New</span></div>
                                    </div>
                                    <div class="notification-info">
                                        <div class="time-info"><small><i class="fa fa-comments"></i> 23rd Dec 2014</small></div>
                                        <h5>Heading </h5>
                                        <p>Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...</p>
                                    </div>                              
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>