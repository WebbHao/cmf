@extends('Layouts.DashBoard')

@section('pageTitle')	 {{$config['title']         or ''}}   @stop
@section('pageSubTitle') {{$config['description']   or ''}}   @stop
@section('pageHeading') @if($obj){{'编辑'.$config['title']}}@else{{'新建'.$config['title']}}@endif   @stop

@section('css')
@parent
<link href="{{asset('wysiwyg/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
{{-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> --}}
{{--<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">--}}
<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link href="{{asset('wysiwyg/editor.css')}}" rel="stylesheet">
<link href="{{asset('css/dropzone.min.css')}}" rel="stylesheet">
@stop

@section('DashBoard-Content')

<!-- PAGE CONTENT BEGINS -->
	<div class="row">

		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-body">

					{!! Form::open([
					'id'     => 'from_edit',
					'url'    => $page['actionPath']?:Request::getQueryString(),
					'method' => $page['actionMethod'],
					'class'  => 'form-horizontal',
					]) !!}
					@foreach($config['items'] as $key=>$item)
						<div class="form-group">
						{{Form::label('ipt_'.$key,array_get($item,'title'),['class'=>'col-sm-2 control-label'])}}
						@if($item['type']=='file')
							<div id="container_{{$key}}" class="col-sm-10">
                                <div class="dropzone "></div>
							</div>
							{{Form::hidden($key,$obj?$obj->$key:'',['class'=>'frin-controller','id'=>"hid_".$key])}}
						@elseif($item['type']=='image')
							<div id="container_{{$key}}" class="col-sm-10">
								<div class="dropzone ">
                                </div>
                                <br />
								<div id="preview_{{$key}}">
									@if(isset($obj)&&$obj->$key)
										<img src="{{$obj->$key}}" onError="this.src='{{asset("/img/default-image.png")}}'" height="100">
									@endif
								</div>
								{{Form::hidden($key,$obj?$obj->$key:'',['class'=>'frin-controller','id'=>"hid_".$key])}}
							</div>
						@elseif(array_get($item,'type')=='textarea')
							<div class="col-sm-10">
							{{Form::textarea($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'style'=>'display:none;'])}}
							<div class="form-control wysiwyg-editor" data="ipt_{{$key}}">{{$obj?$obj->$key:''}}</div>
							</div>
						@elseif(array_get($item,'type')=='hidden')
						{{Form::hidden($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key])}}
						@elseif($item['type']=='password')
							<div class="col-sm-10">
							{{Form::password($key,['class'=>'form-control','id'=>'ipt_'.$key,'placeholder'=>'请输入'.array_get($item,'title')])}}
							</div>
						@elseif($item['type']=='text')
						<div class="col-sm-10">
							{{Form::text($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
						</div>
						@elseif($item['type']=='select')
						<div class="col-sm-10">
							{{Form::select($key,array_get($item,'selectItems'),$obj?$obj->$key:'',['id'=>'ipt_'.$key,'placeholder'=>array_get($item,'title'),'class'=>'form-control','initVal'=>$obj?$obj->$key:''])}}
						</div>
                        @elseif($item['type']=='number')
                        <div class="col-sm-10">
							{{Form::number($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
						</div>
						@elseif($item['type']=='date')
						<div class="col-sm-10">
							{{Form::date($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
						</div>
						@elseif($item['type']=='email')
						<div class="col-sm-10">
							{{Form::email($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
						</div>
                        @elseif($item['type']=='radio')
                            <div class="col-sm-10">
                                <?php $selectIterms = array_get($item,'selectItems',[]); ?>
                                <div class="radio">
                                @foreach($selectIterms as $k => $v)
                                    <?php
                                        $flag = false;
                                        if($obj&&$obj->$key==$k) $flag = true;
                                        elseif(!$obj&&$k=='1') $flag = true;
                                    ?>
                                    <label>{{Form::radio($key, $k, $flag)}} {{$v}}</label>
                                @endforeach
                                </div>
                            </div>
						@endif
						</div>
						@endforeach
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								{{Form::submit('保存',['id'=>'submit','class'=>'btn btn-primary'])}}
								{{Form::reset('重置',['id'=>'reset','class'=>'btn btn-default'])}}
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@stop

@section('js')
@parent

<script type="text/javascript" src="{{asset('wysiwyg/external/jquery.hotkeys.js')}}"></script>
{{-- <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script> --}}
<script type="text/javascript" src="{{asset('wysiwyg/external/google-code-prettify/prettify.js')}}"></script>
<script type="text/javascript" src="{{asset('wysiwyg/bootstrap-wysiwyg.js')}}"></script>
<script type="text/javascript" src="{{asset('js/dropzone.min.js')}}"></script>
<script type="text/javascript">
    var get_countries_url = "{{$get_countries_url or ''}}";
    var get_cities_url = "{{$get_cities_url or ''}}";
	$(function () {
        $("#ipt_continent_id").trigger('change');
        $("#ipt_country_id").trigger('change');

		$('.wysiwyg-editor').initHTML().wysiwyg({ fileUploadError: showErrorAlert} );
    	window.prettyPrint && prettyPrint();

        Dropzone.autoDiscover = false;
        try {
            $(".dropzone").dropzone({
                url: "{{route('file.upload')}}",
                addRemoveLinks: true,
                dictRemoveLinks: "x",
                dictCancelUpload: "x",
                maxFiles: 1,
                maxFilesize: 5,
                acceptedFiles: ".jpeg,.jpg,.png",
                headers:{"X-CSRF-TOKEN":"{{csrf_token()}}"},
                dictDefaultMessage:"拖拽文件到这里或者点击上传文件",
                dictInvalidInputType:"上传的文件类型错误",
                dictFileTooBig:"您上传的文件过大(5MB)",
                dictCancelUpload:"取消",
                dictRemoveFile:"删除",
                dictMaxFilesExceeded:"超过了允许上传的文件数量",
                init: function() {
                    this.on("success", function(file,response) {
                        var removeOBJ   = file._removeLink;
                        var target      = $(removeOBJ).parents("div.col-sm-10").find("input[type='hidden']");
                        target.val(response);
                        $(removeOBJ).parents("div.col-sm-10").find("#preview_national_flag").hide();
                    });
                    this.on("removedfile", function(file) {
                        var target          = $('.dropzone').parent().find("input[type='hidden']");
                        var deleteFilePath  = target.val();
                        $.post(
                            "{{route('file.delete')}}",
                            {deleteFilePath:deleteFilePath},
                            function (result) {
                                if(result.status=='success'){
                                    target.val('');
                                }
                            }
                        );
                    });
                }
            });
        } catch(e) {
            alert('Dropzone.js does not support older browsers!');
        }

        $("#from_edit").on('submit', function () {
            $('.wysiwyg-editor').each(function () {
                textareaID = $(this).attr('data');
                //console.log(textareaID);
                var html = $(this).html();
                //console.log(html);
                $(this).next().html(html);
                $("#"+textareaID).val(html);
            });
        });

	});
</script>
@stop
