@extends('Layouts.DashBoard')

@section('pageTitle')	 {{$config['title']         or ''}}   @stop
@section('pageSubTitle') {{$config['description']   or ''}}   @stop
@section ('pageHeading') {{$config['title']         or ''}}   @stop

@section('DashBoard-Content')
	<div class="row">
		<div class="col-md-12">
			@if(array_get($config,'filter',false) === true)
			<div class="panel panel-primary">
				<div class="panel-body">
					{!! Form::open([
					'id'     => 'ipt_filter',	
					'url' => array_get($config,'router'),
					'class'  => 'form-inline',
					'method' => 'get',
					]) !!}

					@foreach($config['items'] as $key=>$item)
					@if($item['filter'] === true)
						<div class="form-group">
							{{--{{Form::label('ipt_'.$key, $item['title'])}}:--}}
							@if($item['type']=='select'||$item['type']=='radio')
								{{Form::select($key,array_get($item,'selectItems'),array_get($filterData,$key, ''),['id'=>'ipt_'.$key,'placeholder'=>array_get($item,'title'),'class'=>'form-control','initVal'=>array_get($filterData,$key, '')])}}
							@else
								{{Form::text($key,array_get($filterData,$key, ''),['id'=>'ipt_'.$key,'placeholder'=>$item['title'],'class'=>'form-control'])}}
							@endif
						</div>	
					@endif
					@endforeach
					<button type="submit" class="btn btn-primary">查询</button>
					<a href="{{$config['router']}}/create?{{Request::getQueryString()}}">
						<button type="button" class="btn btn-default">新建</button>
					</a>
					{!! Form::close() !!}
					
				</div>
			</div>
			@endif
			<div class="panel panel-primary">
				<div class="panel-body">
					<table class="table">
						<thead>
						<tr>
							@foreach($config['items'] as $key=>$item)
								@if(!isset($item['hidden'])||$item['hidden']===false)
								<th>{{$item['title']}}</th>
								@endif
							@endforeach
							<th>删除/编辑/查看</th>
						</tr>
						</thead>
						<tbody>

						@foreach($data as $value)
						<tr>
							@foreach($config['items'] as $key=>$item)
							@if(!isset($item['hidden'])||$item['hidden']===false)
							@if($item['type']=='image')
							<td>
								@if($value[$key])
								<img src="{{$value[$key]}}" onerror="this.src='{{asset("img/default-image.png")}}'" alt="" width="40"/>
								@endif
							</td>
							@elseif($item['type']=='select'&&$key=='continent_id')
								<td>{{$value->continent_id?$value->continent->name_cn:'--'}}</td>
							@elseif($item['type']=='select'&&$key=='country_id')
								<td>{{is_object($value->country)?$value->country->name_cn:'--'}}</td>
							@elseif($item['type']=='select'&&$key=='city_id')
								<td>{{is_object($value->city)?$value->city->name_cn:'--'}}</td>
							@elseif($item['type']=='select')
							<td>{{array_get($item['selectItems'],$value[$key],'--')}}</td>
							@elseif($item['type']=='radio')
								<td>{{$value->$key?$item['selectItems'][$value[$key]]:'否'}}</td>
							@else
							<td>{{$value[$key]}}</td>
							@endif
							@endif
							@endforeach
							<td>
								<a class="btn btn-warning" href="{{ URL::to($config['router'].'/' . $value->id . '/edit') }}?{{Request::getQueryString()}}">
									编 辑
								</a>
								<a class="btn btn-success" href="{{ URL::to($config['router'].'/'. $value->id) }}?{{Request::getQueryString()}}">
									查 看
								</a>
								{{ Form::open(array('url' => $config['router'].'/' . $value->id.'?'.Request::getQueryString(), 'class' =>
								'pull-left')) }}
								{{ method_field('DELETE') }}
								{{ Form::submit('删 除', array('class' => 'btn btn-danger','style'=>"margin-right: 5px;")) }}
								{{ Form::close() }}&nbsp;&nbsp;
							</td>
						</tr>
						@endforeach
						</tbody>
					</table>
					{{$data->links()}}
				</div>
				
			</div>
		</div>
	</div>
@stop
@section("js")
	<script>
        var get_countries_url = "{{$get_countries_url or ''}}";
        var get_cities_url = "{{$get_cities_url or ''}}";
        $(function () {
            $("#ipt_continent_id").trigger('change');
            $("#ipt_country_id").trigger('change');
        })
	</script>
@stop
