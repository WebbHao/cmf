@extends ('Layouts.Login')
@section('pageTitle') Login @stop
@section('leftContent')
    @parent
@stop

@section('errors')
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>警告!</strong> 登陆过程中有异常.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
@endsection

@section('page-content')

<div class="heading">Login to your Account{{Session::get('theme')}}--</div>
<div class="input">

	<form class="form-horizontal" role="form" method="POST" action="{{ url ('/login') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="login-input-group">
			<span class="login-input-group-addon"><i class="fa fa-at fa-fw"></i></span>
			<input type="email" class="form-control" name="email" placeholder="email" value="{{ old('email') }}">
		</div>
		<div class="login-input-group">
			<span class="login-input-group-addon"><i class="fa fa-key fa-fw"></i></span>
			<input type="password" class="form-control" placeholder="password" name="password">
		</div>
		<button type="submit" class="btn btn-default btn-lg submit">Login</button>
	</form>				
	<div class="login-other text-right">Or Login with
		<a href="javascript:;" class="m-l-sm"><i class="fa fa-facebook"></i></a>
		<a href="javascript:;" class="m-l-sm"><i class="fa fa-twitter"></i></a>
		<a href="javascript:;" class="m-l-sm"><i class="fa fa-google-plus gplus"></i></a>
	</div>
</div>
<div class="rightFooter">
	<a href="{{ url ('/register') }}">Don't have an Account?</a>
	<a href="{{ url('/password/reset') }}" class="m-l">Forgot Password?</a>
</div>

@stop

@section('js')
	@parent
<script type="text/javascript">
	$(".alert-danger").click(function(){
    $(".alert-danger").hide();
});
</script>

@endsection