@extends('Layouts.DashBoard')

@section('pageTitle')	 {{$config['title']         or ''}}   @stop
@section('pageSubTitle') {{$config['description']   or ''}}   @stop
@section ('pageHeading') {{$config['title']         or ''}}   @stop

@section('DashBoard-Content')
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-primary">
				<div class="panel-body">
					{!! Form::open([
					'id'     => 'ipt_filter',	
					'url' => array_get($config,'router'),
					'class'  => 'form-inline',
					'method' => 'get',
					]) !!}
					@if(array_get($config,'filter',false) === true)
					@foreach($config['items'] as $key=>$item)
					@if($item['filter'] === true)
						<div class="form-group">
							{{Form::label('ipt_'.$key, $item['title'])}}:
							@if($item['type']=='select')
								{{Form::select($key,array_get($item,'selectItems'),array_get($filterData,$key, ''),['id'=>'ipt_'.$key,'placeholder'=>'请选择'.array_get($item,'title'),'class'=>'form-control'])}}
							@else
								{{Form::text($key,array_get($filterData,$key, ''),['id'=>'ipt_'.$key,'placeholder'=>$item['title'],'class'=>'form-control'])}}
							@endif
						</div>	
					@endif
					@endforeach
					<button type="submit" class="btn btn-primary">查询</button>
					@endif
					@can('Region\ContinentController@create')
					<a href="{{$config['router']}}/create?{{Request::getQueryString()}}">
						<button type="button" class="btn btn-default">新建</button>
					</a>
					@endcan
					{!! Form::close() !!}
					
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body">
					<table class="table">
						<thead>
						<tr>
							@foreach($config['items'] as $key=>$item)
								@if(!isset($item['hidden'])||$item['hidden']!==true)
								<th>{{$item['title']}}</th>
								@endif
							@endforeach
							<th>删除/编辑/查看</th>
						</tr>
						</thead>
						<tbody>

						@foreach($data as $value)
						<tr>
							@foreach($config['items'] as $key=>$item)
							@if(!isset($item['hidden'])||$item['hidden']!==true)
							@if($item['type']=='image')
							<td>
								@if($value[$key])
								<img src="http://baicheng-cms.qiniudn.com/{{$value[$key]}}-w36" alt=""/>
								@endif
							</td>
							@elseif($item['type']=='select')
							<td>{{array_get($item['selectItems'],$value[$key],'--')}}</td>
							@elseif($item['type']=='radio')
								<td>{{$item['selectItems'][$value[$key]]}}</td>
							@else
							<td>{{$value[$key]}}</td>
							@endif
							@endif
							@endforeach
							<td>
								@can('Region\ContinentController@create')
								<a class="btn btn-warning" href="{{ URL::to($config['router'].'/' . $value->id . '/edit') }}?{{Request::getQueryString()}}">
									编 辑
								</a>
								@endcan

								@can('Region\ContinentController@show')
								<a class="btn btn-success" href="{{ URL::to($config['router'].'/'. $value->id) }}?{{Request::getQueryString()}}">
									查 看
								</a>
								@endcan

								@can('Region\ContinentController@destroy')
								{{ Form::open(array('url' => $config['router'].'/' . $value->id.'?'.Request::getQueryString(), 'class' =>
								'pull-left')) }}
								{{ method_field('DELETE') }}
								{{ Form::submit('删 除', array('class' => 'btn btn-danger','style'=>"margin-right: 5px;")) }}
								{{ Form::close() }}&nbsp;&nbsp;
							    @endcan
							</td>
						</tr>
						@endforeach
						</tbody>
					</table>
					{{$data->links()}}
				</div>
				
			</div>
		</div>
	</div>
@stop
