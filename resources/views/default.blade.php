<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js">
	<![endif]-->
	<!--[if IE 9]>
	<html lang="en" class="ie9 no-js">
		<![endif]-->
		<!--[if !IE]>
		<!-->
		<html lang="en" class="no-js">
			<!--<![endif]-->

			<!-- Mirrored from dashy.strapui.com/laravel/profile by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 26 Aug 2015 11:41:51 GMT -->
			<!-- Added by HTTrack -->
			<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
			<!-- /Added by HTTrack -->
<head>
			<meta charset="utf-8"/>
			<title>CMF | {{Config::get('app.name')}}</title>
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
			<meta content="" name="description"/>
			<meta content="" name="author"/>
			<link rel="stylesheet" href="{{asset('static/css/vendor.css')}}"/>
			<link rel="stylesheet" href="{{asset('static/css/app-green.css')}}"/>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
			<div id="app-container">


			<div id="body-container">

				<div class="conter-wrapper">
					<div class="home-wrapper">
						<div class="cover-wrapper">
							<div class="cover-photo" style="background-image: url( '{{asset('static/img/profile-cover.jpg')}}') ;">
								<div class="name-desg">
									<h3>
										Kumar Sanket
										<small>CEO and Founder @Sahusoft</small>
									</h3>
								</div>
							</div>
							<div class="profile-photo-warp">
								<img class="profile-photo img-responsive img-circle" src="{{asset('static/img/profile1.jpg')}}" alt=""></div>
							<div class="foobar">
								<a href="#">
									<i class="fa fa-heart text-danger"></i>
									443
								</a>
								&nbsp;&nbsp;&nbsp;
								<i class="fa fa-users"></i>
								443
								<span class="probutton">
									<button type="button" class="btn btn-primary  btn-bordered   ">Follow</button>
								</span>
								<span class="links pull-right">
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
									<a href="#">
										<i class="fa fa-google-plus"></i>
									</a>
									<a href="#">
										<i class="fa fa-github"></i>
									</a>
								</span>
							</div>
						</div>
						<div class="conter-wrapper">
							<div>
								<div class="profile-body row" id="profile-items">
									<div class="col-sm-6">
										<div class="profile-comment prophoto">
											<div class="panel panel-default">
												<div class="panel-body">
													<textarea name="" id="" cols="54" rows="4"></textarea>
												</div>
												<div class="panel-footer">
													<div class="submit-footer">
														<a href="#">
															<i class="fa fa-picture-o"></i>
														</a>
														<a href="#">
															<i class="fa fa-calendar"></i>
														</a>
														<a href="#">
															<i class="fa fa-video-camera"></i>
														</a>
													</div>
													<span class="probutton">
														<button type="button" class="btn btn-primary pull-right btn-rounded    ">Send Message</button>
													</span>
												</div>
											</div>
										</div>
										<div class="prophoto">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<img class="panel-photo img-responsive img-circle" src="{{asset('static/img/profile1.jpg')}}" alt="">
														Kumar Sanket
														<br/>
														<span class="text-muted">Posted on 3rd March 2014</span>
													</h3>
												</div>
												<div class="panel-body">
													<img class="img-responsive" src="{{asset('static/img/colorful4.jpg')}}" alt="" height="200">
													<div class="comment-links clearfix">
														<a href="#">
															<i class="fa fa-share-alt"></i>
															22
														</a>
														<a href="#">
															<i class="fa fa-comments-o"></i>
															106
														</a>
														<a href="#">
															<i class="fa fa-heart text-danger"></i>
															862
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="prophoto">
											<div class="comment-link">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h3 class="panel-title">
															<img class="panel-photo img-responsive img-circle" src="{{asset('static/img/profile1.jpg')}}" alt="">
															Kumar Sanket
															<br/>
															<span class="text-muted">Posted on 3rd March 2014</span>
														</h3>
													</div>
													<div class="panel-body">
														<div class="lorem">
															Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur harum aliquid tempore molestias nemo modi quas repellat. Accusantium praesentium, cupiditate tempore culpa voluptate laboriosam itaque error iste accusamus reprehenderit illum! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est saepe voluptas, eligendi necessitatibus adipisci soluta, amet magnam, rerum, iure minima fuga praesentium nobis veniam quisquam illum repellat beatae. Consectetur, asperiores.
														</div>
														<div class="comment-links clearfix">
															<a href="#">
																<i class="fa fa-share-alt"></i>
																22
															</a>
															<a href="#">
																<i class="fa fa-comments-o"></i>
																106
															</a>
															<a href="#">
																<i class="fa fa-heart text-danger"></i>
																862
															</a>
														</div>
														<div class="comments-here media">
															<a class="pull-left" href="#">
																<img class="media-object img-circle img-responsive" src="{{asset('static/img/user-icon.png')}}" alt="Media Object"></a>
															<div class="media-body">
																<a href="#">
																	<h5 class="media-heading">Kumar Pratik</h5>
																</a>
																<span class="timely pull-right text-muted">3 hours ago</span>
																Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic repudiandae exercitationem provident nihil consectetur.
																<div class="comment-like">
																	<a href="#">
																		<i class="fa fa-comments-o"></i>
																		106
																	</a>
																	<a href="#">
																		<i class="fa fa-heart text-danger"></i>
																		862
																	</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class=" col-sm-6 ">
										<div class="prophoto">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<img class="panel-photo img-responsive img-circle" src="{{asset('static/img/profile1.jpg')}}" alt="">
														Kumar Sanket
														<br/>
														<span class="text-muted">Posted on 3rd March 2014</span>
													</h3>
												</div>
												<div class="panel-body">
													<img class="img-responsive" src="{{asset('static/img/colorful4.jpg')}}" alt="" height="200">
													<div class="comment-links clearfix">
														<a href="#">
															<i class="fa fa-share-alt"></i>
															22
														</a>
														<a href="#">
															<i class="fa fa-comments-o"></i>
															106
														</a>
														<a href="#">
															<i class="fa fa-heart text-danger"></i>
															862
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="footer-wrap" class="footer">
					Copyright © 2014 Dashy Theme
					<span class="pull-right">
						<a href="javascript:;">
							<i class="fa fa-facebook-square"></i>
						</a>
						<a href="javascript:;">
							&nbsp;
							<i class="fa fa-twitter-square"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
		<script src="{{asset('static/js/vendor.js')}}" type="text/javascript"></script>
		<script src="{{asset('static/js/app.js')}}" type="text/javascript"></script>
		<script type="text/javascript">

	$(function(){

		// Sidebar Charts

		// Pie Chart
		var chart3 = c3.generate({
		   bindto: '#sidebar-piechart',
		    data: {

		        // iris data from R
		        columns: [
		            ['1', 36],
		            ['2', 54],
		            ['3', 12],
		        ],
		        type : 'pie',
		        onclick: function (d, i) { console.log("onclick", d, i); },
		        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
		        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
		    },
		    color: {
		        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
		    },
		    pie: {
		      expand: true
		    },
		    size: {
		      width: 140,
		      height: 140
		    },
		    tooltip: {
		      show: false
		    }

		});



		// Bar Chart
		var chart6 = c3.generate({
		    bindto: '#sidebar-barchart',
		    data: {
		        columns: [
		            ['data1', 30, 200, 100, 400, 250, 310, 90, 125, 50]
		        ],
		        type: 'bar'
		    },
		    bar: {
		        width: {
		            ratio: 0.8
		        }
		    },
		    size: {
		      width: 200,
		      height: 120
		    },
		    tooltip: {
		      show: false
		    },
		    color: {
		        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
		    },
		    axis: {
		      y: {
		        show: false,
		        color: '#ffffff'
		      }
		}
		});


		// Sidebar Tabs
		$('#navTabs .sidebar-top-nav a').click(function (e) {
		 	e.preventDefault()
		 	$(this).tab('show');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 10);

		});



		$('.subnav-toggle').click(function() {
			$(this).parent('.sidenav-dropdown').toggleClass('show-subnav');
			$(this).find('.fa-angle-down').toggleClass('fa-flip-vertical');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 500);

		});

	    $('.sidenav-toggle').click(function() {
	        $('#app-container').toggleClass('push-right');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 500);

	    });


	    // Boxed Layout Toggle
		$('#boxed-layout').click(function() {

	        $('body').toggleClass('box-section');

	        var hasClass = $('body').hasClass('box-section');

	        $.get('/api/change-layout?layout='+ (hasClass ? 'boxed': 'fluid'));

		});



		$('.tab-content-scroller').perfectScrollbar();

		$('.theme-picker').click(function() {
			changeTheme($(this).attr('data-theme'));
		});


	});

	function changeTheme(theme) {

		$('<link>')
		  .appendTo('head')
		  .attr({type : 'text/css', rel : 'stylesheet'})
		  .attr('href', '/css/app-'+theme+'.css');

		$.get('api/change-theme?theme='+theme);

	}


</script>
</body>

</html>