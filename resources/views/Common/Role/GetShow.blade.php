@extends('Layouts.DashBoard')

@section('pageTitle')	 {{$config['title']         or ''}}   @stop
@section('pageSubTitle') {{$config['description']   or ''}}   @stop
@section ('pageHeading') {{$config['title']         or ''}}   @stop

@section('DashBoard-Content')

<div class="conter-wrapper">
    <div class="row">
        
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$config['title']}}详情</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                    @foreach($config['items'] as $key=>$item)
                        <li class="list-group-item">
                        {{array_get($item,'title','-')}}：
                        @if($item['type']=='file')
                            <div id="container_{{$key}}" class="col-sm-10">
                                <div id="preview_{{$key}}">
                                    @if(isset($data[$key])&&$data[$key])
                                    <img src="http://baicheng-cms.qiniudn.com/{{$data[$key]}}-w100">
                                    @endif
                                </div>
                            </div>    
                        @elseif(array_get($item,'type')=='textarea')
                            {{$obj&&$obj->$key?$obj->$key:'--'}}
                        @elseif($item['type']=='text')
                            {{$obj&&$obj->$key?$obj->$key:'--'}}
                        @elseif($item['type']=='select')
                            {{$obj?array_get(array_get($item,'selectItems'),$obj->$key,'--'):'-'}}
                        @elseif($item['type']=='radio')
                            {{$obj?array_get(array_get($item,'selectItems'),$obj->$key):'--'}}
                        @elseif($item['type']=='number')
                            {{$obj&&$obj->$key?$obj->$key:'--'}}
                        @elseif($item['type']=='date')
                            {{$obj&&$obj->$key?$obj->$key:'--'}}
                        @elseif($item['type']=='email')
                            {{$obj&&$obj->$key?$obj->$key:'--'}}
                        @endif
                        </li>
                        @endforeach
                        @foreach($obj->permissions as $key => $val)
                        <li class="list-group-item">@if(!$key){{'用户的权限：'}}@else{{'----------------'}}@endif{{$val->name}}</li>
                        @endforeach
                        </ul>
                        {{Form::button('返回',['id'=>'back','class'=>'btn btn-primary pull-left'])}}
                </div>
            </div>
        </div>
    </div>
</div>
    
@stop

@section('js')
@parent
<script>
    $(function(){
        $("#back").click(function(){
            window.location.href = "{{array_get($config,'router')}}";
        });
    });
</script>

@stop
