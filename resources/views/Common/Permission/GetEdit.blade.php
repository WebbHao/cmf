@extends('Layouts.DashBoard')

@section('pageTitle')	 {{$config['title']         or ''}}   @stop
@section('pageSubTitle') {{$config['description']   or ''}}   @stop
@section('pageHeading') @if($obj){{'编辑'.$config['title']}}@else{{'注册'.$config['title']}}@endif   @stop

@section('css')
@parent
<link href="{{asset('wysiwyg/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
{{-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> --}}
{{--<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">--}}
<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link href="{{asset('wysiwyg/editor.css')}}" rel="stylesheet">
@stop

@section('DashBoard-Content')

<!-- PAGE CONTENT BEGINS -->
	<div class="row">

		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-body">

					{!! Form::open([
					'id'     => 'from_edit',
					'url'    => $page['actionPath']?:Request::getQueryString(),
					'method' => $page['actionMethod'],
					'class'  => 'form-horizontal',
					]) !!}
					@foreach($config['items'] as $key=>$item)
						<div class="form-group">
						{{Form::label('ipt_'.$key,array_get($item,'title'),['class'=>'col-sm-2 control-label'])}}
						@if($item['type']=='file')
							<div id="container_{{$key}}" class="col-sm-10">
								<a class="btn btn-default btn-lg " id="ipt_{{$key}}" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<sapn>选择文件</sapn>
								</a>

								<div id="preview_{{$key}}">
									@if(isset($data[$key])&&$data[$key])
									<img src="http://baicheng-cms.qiniudn.com/{{$data[$key]}}-w100">
									@endif
								</div>
							</div>
							{{Form::hideden($key,$obj?$obj->$key:'',['class'=>'frin-controller','id'=>"hid_".$key])}}
						@elseif(array_get($item,'type')=='textarea')
							<div class="col-sm-10">
							{{Form::textarea($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'style'=>'display:none;'])}}
							<div class="form-control wysiwyg-editor" data="ipt_{{$key}}">{{$obj?$obj->$key:''}}</div>
							</div>
						@elseif(array_get($item,'type')=='hidden')
						{{Form::hidden($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key])}}
						@elseif($item['type']=='password')
							<div class="col-sm-10">
							{{Form::password($key,['class'=>'form-control','id'=>'ipt_'.$key,'placeholder'=>'请输入'.array_get($item,'title')])}}
							</div>
						@elseif($item['type']=='text')
						<div class="col-sm-10">
							{{Form::text($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
							@if($key=='icon')
							<p class="help-block"><a href="{{url("ui-element/icons")}}" target="_blank">ICON列表</a></p>
							@endif
						</div>
						@elseif($item['type']=='select')
						<div class="col-sm-10">
							{{Form::select($key,array_get($item,'selectItems'),$obj?$obj->$key:'',['id'=>'ipt_'.$key,'placeholder'=>'请选择'.array_get($item,'title'),'class'=>'form-control'])}}
						</div>
                        @elseif($item['type']=='number')
                        <div class="col-sm-10">
							{{Form::number($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
						</div>
						@elseif($item['type']=='date')
						<div class="col-sm-10">
							{{Form::date($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
						</div>
						@elseif($item['type']=='email')
						<div class="col-sm-10">
							{{Form::email($key,$obj?$obj->$key:'',['id'=>'ipt_'.$key,'class'=>'form-control'])}}
						</div>
                        @elseif($item['type']=='radio')
                            <div class="col-sm-10">
                                <?php $selectIterms = array_get($item,'selectItems',[]); ?>
                                <div class="radio">
                                @foreach($selectIterms as $k => $v)
                                    <?php
                                        $flag = false;
                                        if($obj&&$obj->$key==$k) $flag = true;
                                        elseif(!$obj&&$k=='1') $flag = true;
                                    ?>
                                    <label>{{Form::radio($key, $k, $flag)}} {{$v}}</label>
                                @endforeach
                                </div>
                            </div>
						@endif
						</div>
						@endforeach
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								{{Form::submit('保存',['id'=>'submit','class'=>'btn btn-primary'])}}
								{{Form::reset('重置',['id'=>'reset','class'=>'btn btn-default'])}}
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@stop

@section('js')
@parent

<script type="text/javascript" src="{{asset('wysiwyg/external/jquery.hotkeys.js')}}"></script>
{{-- <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script> --}}
<script type="text/javascript" src="{{asset('wysiwyg/external/google-code-prettify/prettify.js')}}"></script>
<script type="text/javascript" src="{{asset('wysiwyg/bootstrap-wysiwyg.js')}}"></script>
<script type="text/javascript">
	$(function () {

		$('.wysiwyg-editor').initHTML().wysiwyg({ fileUploadError: showErrorAlert} );
    	window.prettyPrint && prettyPrint();
        $("#from_edit").on('submit', function () {
            $('.wysiwyg-editor').each(function () {
                textareaID = $(this).attr('data');
                //console.log(textareaID);
                var html = $(this).html();
                //console.log(html);
                $(this).next().html(html);
                $("#"+textareaID).val(html);
            });
        });

		$(".need_uploader").each(function () {
			var name = $(this).attr('name');
			Qiniu.uploader({
				runtimes     : 'html5,flash,html4',
				browse_button: 'ipt_' + name,
				container    : 'container_' + name,
				drop_element : 'container_' + name,
				max_file_size: '100mb',
				flash_swf_url: '/static/js/plupload/Moxie.swf',
				dragdrop     : true,
				chunk_size   : '4mb',
				uptoken_url  : '/file/token',
				domain       : 'http://baicheng-cms.qiniudn.com/',
				auto_start   : true,
				init         : {
					'Key'         : function (up, file) {
						var key = $.ajax({
							url  : "/file/key/master",
							async: false
						}).responseText;
						return key;
					},
					'BeforeUpload': function (up, file) {
						$('#preview_' + name).html('上传中..');
					},
					'FileUploaded': function (up, file, info) {
						var info = $.parseJSON(info);
						if (info.key) {
							$("#hid_" + name).val(info.key);
							$('#preview_' + name).empty().append($('<img/>').attr('src', 'http://baicheng-cms.qiniudn.com/' + info.key + '-w100'));
						} else {
							alert('上传失败');
						}

					}
				}
			});
		});
	});
</script>
@stop
