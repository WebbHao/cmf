@extends('admin.layout.default')

@section('content')

<!-- PAGE CONTENT BEGINS -->
<div class="row-fluid">
	<div class="span12">
		<div class="widget-box">
			<div class="widget-header widget-header-blue widget-header-flat">
				<h4 class="lighter">{{$config['title']}}</h4>
				- <a href="{{$config['router']}}/create?{{Request::getQueryString()}}">创建</a>
			</div>

			<div class="widget-body">

				@if(array_get($config,'filter_item',false) === true)
				<div class="widget-main">
					<form id="frm_edit" role="form" action="{{$page['action_path']}}?{{Request::getQueryString()}}"
						  method="get" class="form-inline">
						@foreach($config['items'] as $key=>$item)
						@if($item['filter'] === true)
							
							@if($item['type']=='select')
							<label class="inline" for="ipt_{{$key}}">{{$item['title']}}:</label>
								<select name="{{$key}}">
									@foreach($item['select-items'] as $select_key=>$select_item)
									@if(isset($filter_data[$key])&&$filter_data[$key]==$select_key)
									<option selected value="{{$select_key}}">{{$select_item}}</option>
									@else
									<option value="{{$select_key}}">{{$select_item}}</option>
									@endif
									@endforeach
								</select>
								&nbsp;
							@else
								<label class="inline" for="ipt_{{$key}}">{{$item['title']}}:</label>
								<input autocomplete="false" value="{{$filter_data[$key] or ''}}" name="{{$key}}"
								   type="{{$item['type']}}"
								   id="ipt_{{$key}}"
								   placeholder="{{$item['title']}}">
								   &nbsp;
							@endif	
						@endif
						@endforeach
						<button type="submit" class="btn btn-sm btn-success btn-next">
							查询
							<i class="icon-arrow-right icon-on-right"></i>
						</button>
					</form>	
				</div>
				@endif

				<div class="widget-main">
					<table class="table table-striped table-bordered">
						<thead>
						<tr>
							@foreach($config['items'] as $key=>$item)
							@if(!isset($item['hidden'])||$item['hidden']!==true)
							<td>{{$item['title']}}</td>
							@endif
							@endforeach
							<td style="width: 132px">查看/编辑/删除</td>
						</tr>
						</thead>
						<tbody>

						@foreach($data as $value)
						<tr>
							@foreach($config['items'] as $key=>$item)
							@if(!isset($item['hidden'])||$item['hidden']!==true)
							@if($item['type']=='image')
							<td>
								@if($value[$key])
								<img src="http://baicheng-cms.qiniudn.com/{{$value[$key]}}-w36" alt=""/>
								@endif
							</td>
							@elseif($item['type']=='select')
							<td>{{$item['select-items'][$value[$key]]}}</td>
                            @elseif($key=='role_id')
                            <td><?php echo Power::roleName($value[$key]);?></td>
							@else
							<td>{{$value[$key]}}</td>
							@endif
							@endif
							@endforeach
							<td>


								<a class="btn btn-minier btn-success"
								   href="{{ URL::to($config['router'].'/'. $value->id) }}?{{Request::getQueryString()}}">
									查看
								</a>
								<a class="btn btn-minier btn-info"
								   href="{{ URL::to($config['router'].'/' . $value->id . '/edit') }}?{{Request::getQueryString()}}">
									编辑
								</a>
								{{ Form::open(array('url' => $config['router'].'/' . $value->id.'?'.Request::getQueryString(), 'class' =>
								'pull-right')) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('删除', array('class' => 'btn btn-minier btn-warning')) }}
								{{ Form::close() }}
							</td>
						</tr>
						@endforeach
						</tbody>
					</table>
					<!-- /widget-main -->
				</div>
				{{$data->links()}}
				<!-- /widget-body -->
			</div>
		</div>
	</div>
</div>

@stop



