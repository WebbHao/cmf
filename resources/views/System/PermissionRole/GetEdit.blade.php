@extends('Layouts.DashBoard')

@section('pageTitle')    {{$config['title']         or ''}}   @stop
@section('pageSubTitle') {{$config['description']   or ''}}   @stop
@section('pageHeading')  {{'编辑角色权限'}}                   @stop
@section('pageSubTitle') {{'为角色分配或修改已有权限信息'}}   @stop 

@section('css')
@parent
<link href="{{asset('wysiwyg/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
{{-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> --}}
{{--<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">--}}
<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link href="{{asset('wysiwyg/editor.css')}}" rel="stylesheet">
@stop

@section('DashBoard-Content')

<!-- PAGE CONTENT BEGINS -->
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$role->name}}            
                        <div class="panel-control pull-right hidden">
                            <a class="panelButton"><i class="fa fa-refresh"></i></a>
                            <a class="panelButton"><i class="fa fa-minus"></i></a>
                            <a class="panelButton"><i class="fa fa-remove"></i></a>
                        </div>
                                </h3>
                </div>
                <div class="panel-body">
                    @if(session('msg'))
                    <div class="alert alert-danger  alert-dismissable " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span></button> 
                         <i class="fa fa-remove"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                         {{session('msg')}}
                    </div>
                    @endif
                    {!! Form::open([
                    'id'     => 'from_edit',
                    'url'    => route('system.pr.save',[$roleID]),
                    'method' => 'post',
                    'class'  => 'form-horizontal',
                    ]) !!}
                    @foreach($permission as $key=>$item)
                        <div class="row">
                        
                        <div class="col-sm-10 checkbox">
                        <label>
                            {{Form::checkbox('permissionIDs[]', $key,in_array($key,$existPermissionList)?true:false),['id'=>'ipt_'.$key]}}
                            {{$item}}
                        </label>    
                        </div>
                        </div>
                        @endforeach
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {{Form::submit('保存',['id'=>'submit','class'=>'btn btn-primary'])}}
                                {{Form::reset('重置',['id'=>'reset','class'=>'btn btn-default'])}}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
@parent

<script type="text/javascript">
    $(function () {

    });
</script>
@stop
