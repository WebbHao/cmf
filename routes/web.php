<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['domain' => Config::get('app.domain')], function () {

	Auth::routes();
	Route::get('/', function () {
	    return view('default');
	});


    Route::group(['prefix' => 'ui-element'], function()
    {
        Route::get('/buttons', function() {
            return View::make('buttons');
        });
        Route::get('/dropdown', function() {
            return View::make('dropdown');
        });
        Route::get('/modal', function() {
            return View::make('modal');
        });
        Route::get('/progressbars', function() {
            return View::make('progressbars');
        });
        Route::get('/alerts', function() {
            return View::make('alerts');
        });
        Route::get('/typography', function() {
            return View::make('typography');
        });
        Route::get('/icons', function() {
            return View::make('icons');
        });
        Route::get('/collapse', function() {
            return View::make('collapse');
        });
        Route::get('/breadcrumbs', function() {
            return View::make('breadcrumbs');
        });
        Route::get('/navbar', function() {
            return View::make('navbar');
        });
        Route::get('/other-elements', function() {
            return View::make('other-elements');
        });
    });

//    Route::get('/', function() {
//        return View::make('home');
//    });

    Route::get('/panel', function() {
        return View::make('panel');
    });

    Route::get('/calendar', function() {
        return View::make('calendar');
    });

    Route::get('/table', function() {
        return View::make('table');
    });

    Route::get('/grid', function() {
        return View::make('grid');
    });

    Route::get('/inbox', function() {
        return View::make('inbox');
    });

    Route::get('/form/elements', function() {
        return View::make('elements');
    });

    Route::get('/form/components', function() {
        return View::make('components');
    });

    Route::get('/chart/c3charts', function() {
        return View::make('c3charts');
    });

    Route::get('/chart/chartjs', function() {
        return View::make('chartjs');
    });

    Route::get('/404', function() {
        return View::make('404');
    });

    Route::get('/stats', function() {
        return View::make('stats');
    });

    Route::get('/paneldocumentation', function() {
        return View::make('paneldocumentation');
    });

    Route::get('/tabledocumentation', function() {
        return View::make('tabledocumentation');
    });

    Route::get('messages/invoice', function() {
        return View::make('invoice');
    });

    Route::get('messages/inbox', function() {
        return View::make('inbox/inbox');
    });

    Route::get('messages/compose', function() {
        return View::make('inbox/compose');
    });


    Route::get('/profile', function() {
        return View::make('profile');
    });


    Route::get('/blank', function() {
        return View::make('blank');
    });


    Route::get('/docs', function() {
        return View::make('docs');
    });


    /**
     * API to set the Session Variable for API
     */

    Route::get('api/change-theme', function(Request $request) {
        \Session::set('theme', $request->input('theme'));
    });

    Route::get('api/change-layout', function(\Request $request) {
        \Session::set('layout', $request->input('layout'));
    });

	Route::group(['middleware'=>'auth'],function(){
		
		Route::get('/home', 'HomeController@index');
        Route::get('/route_list', 'HomeController@routeList');
		Cmf\Http\Controllers\Common\BasicController::initRouter([
            \Cmf\Model\System\User::getConfig(),
			\Cmf\Model\System\Role::getConfig(),
			\Cmf\Model\System\Permission::getConfig(),

            \Cmf\Model\Region\Continent::getConfig(),
            \Cmf\Model\Region\Country::getConfig(),
            \Cmf\Model\Region\City::getConfig(),
            \Cmf\Model\Region\Region::getConfig(),

            \Cmf\Model\Common\Enum::getConfig(),
            \Cmf\Model\Common\Tag::getConfig(),
            \Cmf\Model\Common\Type::getConfig(),

            \Cmf\Model\POI\Poi::getConfig(),
		]);

        //分配权限
        Route::get('/system/permission-role/edit/{editID}','System\PermissionRoleController@edit')->name('system.pr.edit');
        Route::post('/system/permission-role/save/{editID}','System\PermissionRoleController@save')->name('system.pr.save');

        //分配角色
        Route::get('/system/role-user/edit/{userID}','System\RoleUserController@edit')->name('system.ru.edit');
        Route::post('/system/role-user/save/{userID}','System\RoleUserController@save')->name('system.ru.save');


        //公共操作
        Route::any('/file/upload','Common\BaseFileController@upload')->name('file.upload');//文件上传
        Route::any('/file/delete','Common\BaseFileController@delete')->name('file.delete');//文件删除
        Route::any('/common/countries','Common\BaseController@getCountries')->name('common.countries');//获取国家列表
        Route::any('/common/cities','Common\BaseController@getCities')->name('common.cities');//获取城市列表
	});

});

