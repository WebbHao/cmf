<?php
    function arrayFlip($list=[])
    {
        $flipList = [];
        foreach($list as $key => $val)
        {
            $flipList[$val['id']] = $val;
        }
        return $flipList;
    }

    /**
     * @todo   从数组中截取部分元素
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  array  &$list    [数组地址引用]
     * @param  string $parentID [description]
     * @param  string $conditionKey 检索字段
     * @return minxed           [description]
     */
    function chunk(&$list,$parentID,$conditionKey){
        $tmp = $list;
        $recursion = [];
        foreach($tmp as $key => $val){
            if($val[$conditionKey]===$parentID){
                array_push($recursion,$val);
                unset($list[$key]);
            }
        }
        return $recursion;
    };

    /**
     * @todo  对指定的数组递归调用
     *
     * @author Justin.W<justin.w@baicheng.com>
     * @param  array  $list   被递归的数组
     * @param  string $parentsIDs 父级id集合，默认为0
     * @param  string $conditionKey 检索字段
     * @return mixed
     */
    function recursion(&$list,&$recursionList=[],$parentID='0',$conditionKey='parent_id')
    {
        if(is_array($list)&&count($list)){
            
            $subRecursionList = chunk($list,$parentID,$conditionKey);
            if(count($recursionList)){
                // echo '<br />--------------------------<br />';
                // var_dump($recursionList);
                // echo '<br />=========================<br />';
                $recursionList['sub_list'] = $subRecursionList;
            }else{
                $recursionList = $subRecursionList;
            }
            // var_dump($recursionList);
            // var_dump($list);
        }else{
            return $recursionList;
        }
        //$recursionList = [];
        if(is_array($subRecursionList)&&count($subRecursionList)){
            foreach($subRecursionList as $key => $val)
            {   
                
                if(array_key_exists('sub_list',$recursionList)){
                    
                    recursion($list,$recursionList['sub_list'][$key],$val['id']);
                }else{
                    recursion($list,$recursionList[$key],$val['id']);
                }
            }
        }
        return $recursionList;

    }

    $list = [
        ['id'=>'1','name'=>'a1','parent_id'=>'2'],
        ['id'=>'2','name'=>'a2','parent_id'=>'3'],
        ['id'=>'3','name'=>'a3','parent_id'=>'0'],
        ['id'=>'4','name'=>'a4','parent_id'=>'0'],
        ['id'=>'5','name'=>'a5','parent_id'=>'6'],
        ['id'=>'6','name'=>'a6','parent_id'=>'7'],
        ['id'=>'7','name'=>'a7','parent_id'=>'4'],
        ['id'=>'8','name'=>'a8','parent_id'=>'0'],
        ['id'=>'9','name'=>'a9','parent_id'=>'8'],
        ['id'=>'10','name'=>'a10','parent_id'=>'9'],
        ['id'=>'11','name'=>'a11','parent_id'=>'0'],
    ];

   
    $recursionList = [];
    recursion($list,$recursionList,'0');
    echo json_encode($recursionList);
    //var_dump($recursionList);
