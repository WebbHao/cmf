/*
Navicat MySQL Data Transfer

Source Server         : LOCAL
Source Server Version : 50712
Source Host           : 127.0.0.1:33060
Source Database       : homestead

Target Server Type    : MYSQL
Target Server Version : 50712
File Encoding         : 65001

Date: 2017-01-22 18:19:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_permissions
-- ----------------------------
DROP TABLE IF EXISTS `cmf_permissions`;
CREATE TABLE `cmf_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'icon图标',
  `control` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `match_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级权限编号',
  `type_id` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '3' COMMENT '类型(1菜单、2页面、3页面元素)',
  `sort` int(11) DEFAULT NULL COMMENT '排序序号',
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '权限状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cmf_permissions
-- ----------------------------
INSERT INTO `cmf_permissions` VALUES ('1', '注销', '', 'Auth\\LoginController', 'logout', '', 'logout', '', '0', '3', '0', '1', '2017-01-22 15:34:49', '2017-01-22 15:35:56');
INSERT INTO `cmf_permissions` VALUES ('2', '注册', '', 'Auth\\RegisterController', 'showRegistrationForm', '', 'register', '', '0', '3', '0', '1', '2017-01-22 15:34:49', '2017-01-22 15:36:54');
INSERT INTO `cmf_permissions` VALUES ('3', '管理员信息维护', 'fa fa-user ', 'System\\UserController', 'index', '', 'user', '', '36', '1', '1', '1', '2017-01-22 15:34:49', '2017-01-22 16:03:53');
INSERT INTO `cmf_permissions` VALUES ('4', '注册管理员', '', 'System\\UserController', 'create', '', 'user', '', '3', '2', '2', '1', '2017-01-22 15:34:49', '2017-01-22 16:07:55');
INSERT INTO `cmf_permissions` VALUES ('5', '保存管理员信息', '', 'System\\UserController', 'store', '', 'user', '', '4', '3', '0', '1', '2017-01-22 15:34:49', '2017-01-22 15:43:57');
INSERT INTO `cmf_permissions` VALUES ('6', '管理员信息详情', '', 'System\\UserController', 'show', '', 'user', '', '3', '2', '0', '1', '2017-01-22 15:34:49', '2017-01-22 15:44:36');
INSERT INTO `cmf_permissions` VALUES ('7', '编辑管理员信息', '', 'System\\UserController', 'edit', '', 'user', '', '3', '2', '2', '1', '2017-01-22 15:34:49', '2017-01-22 15:45:17');
INSERT INTO `cmf_permissions` VALUES ('8', '更新管理员信息', '', 'System\\UserController', 'update', '', 'user', '', '7', '3', '0', '1', '2017-01-22 15:34:49', '2017-01-22 15:45:47');
INSERT INTO `cmf_permissions` VALUES ('9', '注销管理员信息', '', 'System\\UserController', 'destroy', '', 'user', '', '0', '3', '0', '1', '2017-01-22 15:34:49', '2017-01-22 15:46:17');
INSERT INTO `cmf_permissions` VALUES ('10', '角色信息维护', 'fa fa-group', 'System\\RoleController', 'index', '', 'system', '', '36', '1', '3', '1', '2017-01-22 15:34:49', '2017-01-22 16:04:41');
INSERT INTO `cmf_permissions` VALUES ('11', '注册角色', 'fa fa-group', 'System\\RoleController', 'create', '', 'role', '', '10', '2', '1', '1', '2017-01-22 15:34:49', '2017-01-22 16:10:38');
INSERT INTO `cmf_permissions` VALUES ('12', '保存角色', 'fa fa-group', 'System\\RoleController', 'store', '', 'role', '', '11', '3', '2', '1', '2017-01-22 15:34:49', '2017-01-22 16:13:18');
INSERT INTO `cmf_permissions` VALUES ('13', '角色详情', 'fa fa-group', 'System\\RoleController', 'show', '', 'role', '', '10', '2', '3', '1', '2017-01-22 15:34:49', '2017-01-22 16:13:50');
INSERT INTO `cmf_permissions` VALUES ('14', '编辑角色', 'fa fa-group', 'System\\RoleController', 'edit', '', 'role', '', '10', '2', '2', '1', '2017-01-22 15:34:49', '2017-01-22 16:11:47');
INSERT INTO `cmf_permissions` VALUES ('15', '更新角色', 'fa fa-group', 'System\\RoleController', 'update', '', 'role', '', '14', '3', '1', '1', '2017-01-22 15:34:49', '2017-01-22 16:12:39');
INSERT INTO `cmf_permissions` VALUES ('16', '注销角色', 'fa fa-group', 'System\\RoleController', 'destroy', '', 'role', '', '10', '3', '4', '1', '2017-01-22 15:34:49', '2017-01-22 16:14:34');
INSERT INTO `cmf_permissions` VALUES ('17', '权限信息维护', 'fa fa-ticket', 'System\\PermissionController', 'index', '', 'permission', '', '36', '1', '2', '1', '2017-01-22 15:34:49', '2017-01-22 16:06:05');
INSERT INTO `cmf_permissions` VALUES ('18', '注册权限', 'fa fa-ticket', 'System\\PermissionController', 'create', '', 'permission', '', '17', '2', '1', '1', '2017-01-22 15:34:49', '2017-01-22 16:21:32');
INSERT INTO `cmf_permissions` VALUES ('19', '保存权限信息', 'fa fa-ticket', 'System\\PermissionController', 'store', '', 'permission', '', '18', '3', '2', '1', '2017-01-22 15:34:49', '2017-01-22 16:22:21');
INSERT INTO `cmf_permissions` VALUES ('20', '权限详情', 'fa fa-ticket', 'System\\PermissionController', 'show', '', 'permission', '', '17', '2', '5', '1', '2017-01-22 15:34:49', '2017-01-22 16:24:11');
INSERT INTO `cmf_permissions` VALUES ('21', '编辑权限', 'fa fa-ticket', 'System\\PermissionController', 'edit', '', 'permission', '', '17', '2', '3', '1', '2017-01-22 15:34:49', '2017-01-22 16:23:03');
INSERT INTO `cmf_permissions` VALUES ('22', '更新权限', 'fa fa-ticket', 'System\\PermissionController', 'update', '', 'permission', '', '21', '3', '4', '1', '2017-01-22 15:34:49', '2017-01-22 16:23:36');
INSERT INTO `cmf_permissions` VALUES ('23', '注销权限', 'fa fa-ticket', 'System\\PermissionController', 'destroy', '', 'permission', '', '17', '3', '6', '1', '2017-01-22 15:34:49', '2017-01-22 16:24:51');
INSERT INTO `cmf_permissions` VALUES ('24', '大洲信息', 'fa fa-align-left ', 'Region\\ContinentController', 'index', '', 'continent', '', '38', '1', '1', '1', '2017-01-22 15:34:49', '2017-01-22 16:31:39');
INSERT INTO `cmf_permissions` VALUES ('25', '新建洲信息', 'fa fa-align-left', 'Region\\ContinentController', 'create', '', 'continent', '', '24', '2', '2', '1', '2017-01-22 15:34:49', '2017-01-22 16:32:54');
INSERT INTO `cmf_permissions` VALUES ('26', '保存洲信息', 'fa fa-align-left', 'Region\\ContinentController', 'store', '', 'continent', '', '25', '3', '2', '1', '2017-01-22 15:34:49', '2017-01-22 16:33:35');
INSERT INTO `cmf_permissions` VALUES ('27', '洲信息详情', 'fa fa-align-left', 'Region\\ContinentController', 'show', '', 'continent', '', '38', '2', '6', '1', '2017-01-22 15:34:49', '2017-01-22 16:41:51');
INSERT INTO `cmf_permissions` VALUES ('28', '编辑洲信息', 'fa fa-align-left', 'Region\\ContinentController', 'edit', '', 'continent', '', '0', '2', '4', '1', '2017-01-22 15:34:49', '2017-01-22 16:35:35');
INSERT INTO `cmf_permissions` VALUES ('29', '更新洲信息', 'fa fa-align-left', 'Region\\ContinentController', 'update', '', 'continent', '', '33', '3', '4', '1', '2017-01-22 15:34:49', '2017-01-22 16:36:19');
INSERT INTO `cmf_permissions` VALUES ('30', '删除洲信息', 'fa fa-align-left', 'Region\\ContinentController', 'destroy', '', 'continent', '', '24', '3', '5', '1', '2017-01-22 15:34:49', '2017-01-22 16:37:15');
INSERT INTO `cmf_permissions` VALUES ('31', '编辑角色权限列表', 'fa fa-group', 'System\\PermissionRoleController', 'edit', '', 'role', '', '14', '3', '5', '1', '2017-01-22 15:34:49', '2017-01-22 16:17:39');
INSERT INTO `cmf_permissions` VALUES ('32', '保存角色权限信息', 'fa fa-group', 'System\\PermissionRoleController', 'save', '', 'system', '', '31', '3', '7', '1', '2017-01-22 15:34:49', '2017-01-22 16:18:32');
INSERT INTO `cmf_permissions` VALUES ('33', '编辑洲信息', 'fa fa-align-left', 'System\\RoleUserController', 'edit', '', 'system', '', '24', '2', '3', '1', '2017-01-22 15:34:49', '2017-01-22 16:34:39');
INSERT INTO `cmf_permissions` VALUES ('34', '保存用户角色', '', 'System\\RoleUserController', 'save', '', 'system', '', '7', '3', '8', '1', '2017-01-22 15:34:49', '2017-01-22 16:42:58');
INSERT INTO `cmf_permissions` VALUES ('35', '控制面板', 'fa fa-home', 'HomeController', 'index', '', 'home', '', '0', '1', '1', '1', '2017-01-22 15:57:46', '2017-01-22 15:57:46');
INSERT INTO `cmf_permissions` VALUES ('36', '系统基础信息', 'fa fa-gears', '', '', '', 'system', '', '0', '1', '2', '1', '2017-01-22 16:00:19', '2017-01-22 16:28:02');
INSERT INTO `cmf_permissions` VALUES ('38', '全球地理信息', 'fa  fa-globe ', '', '', '', 'region', '', '0', '1', '4', '1', '2017-01-22 16:29:39', '2017-01-22 16:29:39');
