$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        'async':false
    });
	$(".loader-block").hide();
	$('.fa.fa-power-off').parent('a').on('click',function(){
		$.post('/logout',function(html){
			window.location.href = '/';
		})
	});
    $("#reset").click(function(){
        window.history.go(-1);
    });
    // $("select").select2({
    //     theme: "classic"
    // });

	//大洲选择
    $("#ipt_continent_id").on("change",function () {
        var continent_id = $(this).val();
        if(get_countries_url) {
            $.ajax({
                type: "post",
                url: get_countries_url,
                beforeSend: function () {
                    $(".loader-block").show();
                },
                data: {continentID: continent_id},
                error:function () {
                    alert('请求异常，请稍后重试');
                    $(".loader-block").hide();
                },
                success: function (data) {
                    $(".loader-block").hide();
                    updateOption('ipt_country_id',data);
                }
            });
        }
    });

    //国家选择
    $("#ipt_country_id").on("change",function () {
        var country_id = $(this).val();
        if(get_countries_url) {
            $.ajax({
                type: "post",
                url: get_cities_url,
                beforeSend: function () {
                    $(".loader-block").show();
                },
                data: {countryID: country_id},
                error:function () {
                    alert('请求异常，请稍后重试');
                    $(".loader-block").hide();
                },
                success: function (data) {
                    $(".loader-block").hide();
                    updateOption('ipt_city_id',data);
                }
            });
        }
    });
});

//更新select列表
function updateOption(id,obj) {
    var text = $("#"+id).text();
    var init = $("#"+id).attr('initVal');
    var html = '<option value="">'+text+'</option>';
    if(obj.length){
        $("#"+id).empty();
        $.each(obj,function(i,v){
            if(init&&init==v.id){
                html += "<option value='" + v.id + "' selected='selected'>" + v.name_cn + "</option>";
            }else {
                html += "<option value='" + v.id + "'>" + v.name_cn + "</option>";
            }
        });
        $("#"+id).append(html);
    }
}