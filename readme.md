# CMF

##项目简介
以laravel5.3为基础，封装常用操作，提供公用的功能模块。模块化代码，按照业务需求快速搭建企业级应用平台。

##模板变量

$title--页面标题<br />
$keywords--页面关键字<br />
$description--页面简洁<br />
$htmlContentId--主体内容DIV的ID，如果不设置该变量，默认为body-container<br />

## 类包说明
app/Http/Contracts  接口包
app/Http/Controller 控制器类
app/Http/Controller/Common   基础控制器类包，
app/Http/Controller/System   系统基础配置
app/Http/Controller/Region   地理信息控制

## CURD公用模块说明
-- 该模块目前一个Form仅支持一个文件上传


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
