<?php

namespace Cmf\Http\Controllers\POI;

use Illuminate\Http\Request;
use View;
use Cmf\Http\Controllers\Common\BasicController;

class PoiController extends BasicController
{
    public function __construct()
    {
        parent::__construct();
        $get_countries_url = route('common.countries');
        $get_cities_url    = route('common.cities');
        View::share('get_countries_url',$get_countries_url);
        View::share('get_cities_url',$get_cities_url);
    }
}
