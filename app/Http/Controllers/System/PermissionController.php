<?php

namespace Cmf\Http\Controllers\System;

use Route,URL,View,Config,Auth,Validator,Redirect,Session;

use Cmf\Http\Requests\BasicRequest as Request;
use Cmf\Http\Controllers\Common\BasicController;

class PermissionController extends BasicController
{
    public function __construct()
    {
        parent::__construct();
        $class = $this->_model;
        $this->admin_config = $class::getConfig();
    }

    /**
     * Store a newly created resource in storage.
     * POST /tests
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $class     = $this->_model;
        $config    = $class::getConfig();
        $rules     = $this->getRules($config);
        $data      = $request->all();
        if(array_has($data,'parent_id')&&!array_get($data,'parent_id')) $data['parent_id'] = '0';
        if(array_has($data,'sort')&&!array_get($data,'sort')) $data['sort'] = '0';
        $user_info = Auth::getUser()->toArray();
        if(is_array($user_info)&&count($user_info)){
            $data['author_id']   = array_get($user_info,'id','');
            $data['author']      = array_get($user_info,'name','');
            $data['operator_id'] = array_get($user_info,'id','');
            $data['operator']    = array_get($user_info,'name','');
        }
        $data['created_at'] = date('Y-m-d H:i:s',time());
        $data['updated_at'] = date('Y-m-d H:i:s',time());
        if (count($rules)) {
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $messages = [];
                foreach ($validator->messages()->all() as $message) {
                    $messages[] = [
                        'class' => 'danger',
                        'text'  => $message,
                    ];
                }
                Session::flash('messages', $messages);
                return Redirect::back()->withInput();
            }
        }

        $obj   = new $class;
        $this->saveObject($obj, $data, $config);

        return Redirect::intended($config['router'] . '?' . $request->getQueryString());
    }

    /**
     * Update the specified resource in storage.
     * PUT /tests/{id}
     *
     * @param  Request $request 请求对象
     * @param  int $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        //dd($request->all());
        $class     = $this->_model;
        $config    = $class::getConfig();
        $rules     = $this->getRules($config);
        $data      = $request->all();
        $user_info = Auth::getUser()->toArray();
        if(array_has($data,'parent_id')&&!array_get($data,'parent_id')) $data['parent_id'] = '0';
        if(array_has($data,'sort')&&!array_get($data,'sort')) $data['sort'] = '0';
        //dd($data);
        if(is_array($user_info)&&count($user_info)){
            $data['author_id']   = array_get($user_info,'id','');
            $data['author']      = array_get($user_info,'name','');
            $data['operator_id'] = array_get($user_info,'id','');
            $data['operator']    = array_get($user_info,'name','');
        }
        $data['updated_at'] = date('Y-m-d H:i:s',time());

        if (count($rules)) {
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $messages = [];
                foreach ($validator->messages()->all() as $message) {
                    $messages[] = [
                        'class' => 'danger',
                        'text'  => $message,
                    ];
                }
                Session::flash('messages', $messages);

                return Redirect::to($config['router'] . '/' . $id . '/edit');
            }

        }


        $obj = $class::find($id);

        $this->saveObject($obj, $data, $config);

        return Redirect::intended($config['router'] . '?' . $request->getQueryString());
    }
}
