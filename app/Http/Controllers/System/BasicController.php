<?php

namespace Cmf\Http\Controllers\System;

use Route,URL,View,Log;

use Cmf\Http\Requests\BasicRequest as Request;
use Cmf\Http\Controllers\Controller;

class BasicController extends Controller
{
	protected $_model = '';

    public function __construct()
    {
        parent::__construct();
        $name = Route::currentRouteName();
        $currentAction = Route::getCurrentRoute()->getAction();
        if($name&&strpos($name,'.')!==false){
            list($class,$action) = explode('.',$name);
            $prefix = array_get($currentAction,'prefix');
            if($prefix) $prefix = substr($prefix,1);
            //if($prefix&&$prefix!=='common'){
                $this->_model = "Cmf\Model\\".ucfirst($prefix).'\\'.ucfirst($class);
            //}else{
            //    $this->_model = "Cmf\Model\\".ucfirst($class);
            //}
        }else{
            $this->_model = "Cmf\Model\User";
        }
        
        //$this->setModel( $model);
    }

    /**
     * @todo    设置$_model
     * 
     * @author  Justin.W<justin.bj@msn.c>
     * @param   Illuminate\Database\Eloquent\Model $model 模型
     */
    public function setModel(\Illuminate\Database\Eloquent\Model $model)
    {
    	$this->_model = $model;
    }

    public function getModel()
    {
    	return $this->_model;
    }

    /**
     * @todo   共享变量
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @param   array $config 变量列表
     * @return  null
     */
    public static function shareVar($config)
    {
        $title       = array_get($config,'title','');
        $keywords    = array_get($config,'keywords','');
        $description = array_get($config,'description','');
        View::share('title',       $title);
        View::share('keywords',    $keywords);
        View::share('description', $description);
    }
}
