<?php

namespace Cmf\Http\Controllers\System;

use Route,URL,View,Config,Auth,Validator,Redirect,Session;

use Cmf\Http\Requests\BasicRequest as Request;
use Cmf\Http\Controllers\Common\BasicController;

class RoleController extends BasicController
{
	public function __construct()
	{
		parent::__construct();
		$class = $this->_model;
        $this->admin_config = $class::getConfig();
	}

	

	/**
	 * Display the specified resource.
	 * GET /tests/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show(Request $request,$id)
	{
		$class      = $this->_model;
		$config     = $class::getConfig();
		$obj        = $class::with('permissions')->find($id);
		$reqData    = $request->all();

		$template   = array_get($config,'templateShow','Common.Basic.GetShow');

		# Display
		static::shareVar($config);

		$data       = [
			'config' => $config,
			'data'   => $reqData,
			'obj'    => $obj,
		];

		if($request->get('debug')=='cmf'&&config::get('app.debug',false))
		{
			dd($data);
		}

		return View::make($template, $data);
	}

}
