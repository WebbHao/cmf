<?php

namespace Cmf\Http\Controllers\System;

use Route,URL,View,Config,Auth,Validator,Redirect,Session;

use Cmf\Http\Requests\BasicRequest as Request;
use Cmf\Http\Controllers\Controller;
use Cmf\Model\RoleUser;
use Cmf\Model\User;
use Cmf\Model\Role;


class RoleUserController extends BasicController
{

    public function edit(Request $request,$userID)
    {
        $user          = User::find($userID);
        $exist         = RoleUser::where('user_id',$userID)->get()->toArray();  
        $existRoleList = array_pluck($exist,'role_id');      
        $roleList      = Role::where('status','1')->get();
        $data          = compact('userID','user','existRoleList','roleList');
        if($request->input('debug','')=='cmf'){
            dd($data);
        }

        return view('System.RoleUser.GetEdit',$data);
    }

    /**
     * @todo 保存分配的权限
     *
     * @author Justin.W<justin.bj@msn.com>
     */
    public function save(Request $request,$userID)
    {
        if(!$userID){
            throw new \Exception('角色参数不能为空');
        }
        $roleIDs = $request->input('roleIDs');
        if(empty($roleIDs)){
            return redirect()->action('System\RoleUserController@edit', [$userID])->withInput()->with('msg','请选择要分配的角色');
        }

        $list = [];
        RoleUser::where('user_id',$userID)->delete();
        foreach($roleIDs as $key => $val){
            $tmp['user_id']       = $userID;
            $tmp['role_id']       = $val;
            array_push($list,$tmp);
        }
        //dd($list);
        RoleUser::insert($list);
        return redirect()->action('System\RoleUserController@edit', [$userID])->with('msg','保存成功');
    }
}
