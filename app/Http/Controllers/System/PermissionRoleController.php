<?php

namespace Cmf\Http\Controllers\System;

use Route,URL,View,Config,Auth,Validator,Redirect,Session;

use Cmf\Http\Requests\BasicRequest as Request;
use Cmf\Http\Controllers\Controller;
use Cmf\Model\PermissionRole;
use Cmf\Model\System\Permission;
use Cmf\Model\System\Role;


class PermissionRoleController extends BasicController
{

    public function edit(Request $request,$roleID)
    {
        $role       = Role::find($roleID);
        $exist      = PermissionRole::where('role_id',$roleID)->get()->toArray();  
        $existPermissionList = array_pluck($exist,'permission_id');      
        $permission = Permission::getPermissionMenuList();
        $data       = compact('roleID','role','existPermissionList','permission');
        if($request->input('debug','')=='cmf'){
            dd($data);
        }

        return view('System.PermissionRole.GetEdit',$data);
    }

    /**
     * @todo 保存分配的权限
     *
     * @author Justin.W<justin.bj@msn.com>
     */
    public function save(Request $request,$roleID)
    {
        if(!$roleID){
            throw new \Exception('角色参数不能为空');
        }
        $permissionIDs = $request->input('permissionIDs');
        if(empty($permissionIDs)){
            return redirect()->action('System\PermissionRoleController@edit', [$roleID])->withInput()->with('msg','请选择要分配的权限');
        }

        $list = [];
        PermissionRole::where('role_id',$roleID)->delete();
        foreach($permissionIDs as $key => $val){
            $tmp['role_id']       = $roleID;
            $tmp['permission_id'] = $val;
            array_push($list,$tmp);
        }
        //dd($list);
        PermissionRole::insert($list);
        return redirect()->action('System\PermissionRoleController@edit', [$roleID])->with('msg','保存成功');
    }
}
