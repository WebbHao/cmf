<?php

namespace Cmf\Http\Controllers\Common;

use Illuminate\Http\Request;

use Cmf\Http\Controllers\Controller;
use Cmf\Model\Region\Country;
use Cmf\Model\Region\City;

class BaseController extends Controller
{
    public $_cols   = ['id','name_cn'];
    public $_status = '1';

    /**
     * @todo  获取指定大洲下的国家列表
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param Request $request
     * @return mixed
     */
    public function getCountries(Request $request)
    {
        $continentID = $request->get('continentID');
        $counties    = '';
        if($continentID){
            $counties = Country::select($this->_cols)->where('continent_id',$continentID)
                ->where('status',$this->_status)->get();
        }
        return $counties;
    }

    /**
     * @todo  获取指定国家下的城市列表
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param Request $request
     * @return mixed
     */
    public function getCities(Request $request)
    {
        $countryID      = $request->get('countryID');
        $cities         = '';
        if($countryID){
            $cities  = City::select($this->_cols)->where('country_id',$countryID)
                ->where('status',$this->_status)->get();
        }
        return $cities;
    }
}
