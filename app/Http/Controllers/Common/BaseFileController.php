<?php

namespace Cmf\Http\Controllers\Common;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cmf\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;

class BaseFileController extends Controller
{
    public $prefixLength  = 6;
    public $prefix        = "+";
    public $randomStart   = 1000;
    public $randomEnd     = 9999;
    public $basePath      = '';

    public function __construct()
    {
        $this->basePath    = Config::get('app.domain').'/static/file/';
    }

    /**
     * @todo   上传文件
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  Request $request
     * @param  Response $response
     * @return mixed
     */
    public function upload(Request $request,Response $response)
    {
        $file      = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        $filename  = str_pad($request->user()->id,$this->prefixLength,$this->prefix);
        $filename  = $filename.time().rand($this->randomStart,$this->randomEnd).'.'.$extension;
        $path      = $file->storeAs(date('Y-m-d'),$filename);
        $url       = $this->basePath.$path;

        return $response->setContent($url);
    }

    /**
     * @todo   删除文件
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  Request  $request
     * @param  Response $response
     * @return mixed
     */
    public function delete(Request $request,Response $response)
    {
        $result = [
            'status'  => 'success',
            'message' => '',
        ];
        $deleteFilePath = $request->get('deleteFilePath','');
        if($deleteFilePath){
            $deleteFilePath = str_replace($this->basePath,'',$deleteFilePath);
            Storage::delete($deleteFilePath);
        }else{
            $result['status'] = 'fail';
            $result['message'] = '文件不存在';
        }

        return $response->setContent($result);
    }
}
