<?php

namespace Cmf\Http\Controllers\Common;

use Route,URL,View,Config,Auth,Validator,Redirect,Session;

use Cmf\Http\Requests\BasicRequest as Request;
use Cmf\Http\Controllers\System\BasicController as SBasicController;

class BasicController extends SBasicController
{
	public function __construct()
	{
		parent::__construct();
		$class = $this->_model;
        $this->admin_config = $class::getConfig();
	}

	public static function initRouter($configs)
	{
		if (count($configs)) foreach ($configs as $config) {
			Route::resource($config['router'], $config['controller']);
		}
	}

	/**
	 * @todo   共享变量
	 *
	 * @author  Justin.W<justin.bj@msn.com>
	 * @param   array $config 变量列表
	 * @return  null
	 */
	public static function shareVar($config)
	{
		$title       = array_get($config,'title','');
		$keywords    = array_get($config,'keywords','');
		$description = array_get($config,'description','');
		View::share('title',       $title);
		View::share('keywords',    $keywords);
		View::share('description', $description);
	}

    public function index(Request $request)
    {
		$class      = $this->_model;
		$config     = $class::getConfig();
		$template   = array_get($config,'templateIndex','Common.Basic.GetIndex');
		$filterData = $request->all();
		$filterDb   = [];
		if(array_get($config,'filter',false)===true&&count($filterData)){
			foreach($config['items'] as $key => $val){
				$filterValue  =  array_get($filterData,$key,'');
				if($val['filter'] === true && $filterValue!==''){
					$filterDb[$key] = $filterValue;
				}
			}

		}
		//pageHeading pageSubTitle
        # Feature 排序
		$orderBy     = array_get($config,'orderBy','id');
		$orderMethod = array_get($config,'orderMethod','desc');
		$pageSize    = array_get($config,'pageSize',env('APP_PAGE_SIZE'));
		$data        = $class::where($filterDb)->orderBy($orderBy,$orderMethod)->paginate($pageSize);

		$htmlData = [
			'page'          => [],
			'data'          => $data,
			'config'        => $config,
			'filterData'    => $filterData,
		];
		if($request->input('debug','')=='cmf'){
			dd($htmlData);
		}
        # Display
		static::shareVar($config);
		return View::make($template, $htmlData);
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /tests/create
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$class        = $this->_model;
		$config       = $class::getConfig();
		$template     = array_get($config,'templateEdit','Common.Basic.GetEdit');

		$data         = [
			'page'   => [
				'actionPath'    => $config['router'],
				'actionMethod'  => 'post',
				'scripts'       => [
					'markdown/markdown.min.js',
					'markdown/bootstrap-markdown.min.js',
					'jquery.hotkeys.min.js',
					'uncompressed/bootstrap-wysiwyg.js',
				],
			],
			'data'   => $request->all(),
			'obj'    => '',
			'config' => $config,
		];

		# Display
		static::shareVar($config);

		if($request->get('debug')=='cmf'&&config::get('app.debug',false))
		{
			dd($data);
		}
		return View::make($template, $data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tests
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$class     = $this->_model;
		$config    = $class::getConfig();
		$rules     = $this->getRules($config);
		$data      = $request->all();
		$user_info = Auth::getUser()->toArray();
		if(is_array($user_info)&&count($user_info)){
			$data['author_id']   = array_get($user_info,'id','');
			$data['author']      = array_get($user_info,'name','');
			$data['operator_id'] = array_get($user_info,'id','');
			$data['operator']    = array_get($user_info,'name','');
		}
		$data['created_at'] = date('Y-m-d H:i:s',time());
		$data['updated_at'] = date('Y-m-d H:i:s',time());
		if (count($rules)) {
			$validator = Validator::make($data, $rules);

			if ($validator->fails()) {
				$messages = [];
				foreach ($validator->messages()->all() as $message) {
					$messages[] = [
						'class' => 'danger',
						'text'  => $message,
					];
				}
				Session::flash('messages', $messages);
				return Redirect::back()->withInput();
			}
		}

		$obj   = new $class;
		$this->saveObject($obj, $data, $config);

		return Redirect::intended($config['router'] . '?' . $request->getQueryString());
	}

	/**
	 * Display the specified resource.
	 * GET /tests/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show(Request $request,$id)
	{
		$class      = $this->_model;
		$config     = $class::getConfig();
		$obj        = $class::find($id);
		$reqData    = $request->all();

		$template   = array_get($config,'templateShow','Common.Basic.GetShow');

		# Display
		static::shareVar($config);

		$data       = [
			'config' => $config,
			'data'   => $reqData,
			'obj'    => $obj,
		];

		if($request->get('debug')=='cmf'&&config::get('app.debug',false))
		{
			dd($data);
		}

		return View::make($template, $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tests/{id}/edit
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit(Request $request,$id)
	{
		$class      = $this->_model;
		$pathinfo   = str_replace('/edit', '', $request->getPathInfo());
		$config     = $class::getConfig();
		$actionPath = isset($config['storePath']) ? : $pathinfo;
		$obj        = $class::find($id);
		$reqData    = $request->all();

		$template   = isset($config['templateEdit']) ? $config['templateEdit'] : 'Common.Basic.GetEdit';

		# Display
		static::shareVar($config);

		$data       = [
			'config' => $config,
			'data'   => $reqData,
			'obj'    => $obj,
			'page'   => [
				'actionPath'   => $actionPath,
				'actionMethod' => 'put',
			],

		];

		if($request->get('debug')=='cmf'&&config::get('app.debug',false))
		{
			dd($data);
		}
		return View::make($template, $data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tests/{id}
	 *
	 * @param  Request $request 请求对象
	 * @param  int $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
 		//dd($request->all());
		$class     = $this->_model;
		$config    = $class::getConfig();
		$rules     = $this->getRules($config);
		$data      = $request->all();
		$user_info = Auth::getUser()->toArray();
		//dd($data);
		if(is_array($user_info)&&count($user_info)){
			$data['author_id']   = array_get($user_info,'id','');
			$data['author']      = array_get($user_info,'name','');
			$data['operator_id'] = array_get($user_info,'id','');
			$data['operator']    = array_get($user_info,'name','');
		}
		$data['updated_at'] = date('Y-m-d H:i:s',time());

		if (count($rules)) {
			$validator = Validator::make($data, $rules);
			if ($validator->fails()) {
				$messages = [];
				foreach ($validator->messages()->all() as $message) {
					$messages[] = [
						'class' => 'danger',
						'text'  => $message,
					];
				}
				Session::flash('messages', $messages);

				return Redirect::to($config['router'] . '/' . $id . '/edit');
			}

		}


		$obj = $class::find($id);

		$this->saveObject($obj, $data, $config);

		return Redirect::intended($config['router'] . '?' . $request->getQueryString());
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tests/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy(Request $request,$id)
	{

		$class        = $this->_model;
		$admin_config = $class::getConfig();
		$obj          = $class::find($id);
		$obj->delete();

		// redirect
		Session::flash('messages', [
			[
				'class' => 'info',
				'text'  => 'Successfully deleted!',
			]
		]);

		return Redirect::to($admin_config['router'] . '?' . $request->getQueryString());
	}

	protected function getRules($config)
	{
		$rules = [];
		foreach ($config['items'] as $key => $item) {
			if (isset($item['validator'])) {
				$rules[$key] = $item['validator'];
			}
		}

		return $rules;

	}

	protected function saveObject($obj, $data, $admin_config)
	{
		foreach ($admin_config['items'] as $key => $value) {
			if ($value['type'] === 'password') {
				if ($data[$key] != '') {
					$obj[$key] = bcrypt($data[$key]);
				} else {
					unset($obj[$key]);
				}


			} elseif ($value['type'] === 'plus_s') {
				$plus_structure_k = Input::get($key . '_k');
				$plus_structure_v = Input::get($key . '_v');
				$plus_structure   = [];
				if (count($plus_structure_v) === count($plus_structure_k)) {
					if ($plus_structure_k) {
						foreach ($plus_structure_k as $k => $v) {
							$plus_structure[$v] = $plus_structure_v[$k];
						}
					}
				}
				$obj[$key] = ($plus_structure);
			} elseif ($value['type'] === 'plus_d') {
				$plus_structure_k = Input::get($key . '_k');
				$plus_structure_v = Input::get($key . '_v');
				$plus_structure   = [];
				if (count($plus_structure_v) === count($plus_structure_k)) {
					if ($plus_structure_k) {
						foreach ($plus_structure_k as $k => $v) {
							$plus_structure[$v] = $plus_structure_v[$k];
						}
					}
				}
				$obj[$key] = ($plus_structure);
			} else {
				switch($key)
                {
                    case 'editor':
                        $obj[$key] = $user['username'];
                        break;
                    case 'channel':
                    // print_r($data);die;
                        if(isset($data['type'])){
                        	 $obj['channel'] = $data['channel'];
                             $obj['type'] = $data['type'];
                        }else{
                        	$countryArr = explode('-',$data['channel']);
                            $obj['channel_id'] = $countryArr['0'];
                            $obj['channel'] = $countryArr['1'];
                        }
                        break;
                    default:
                        $obj[$key] =$data[$key];
                        break;
                }
			}
		}
		$obj->save();

		return $obj;
	}

}
