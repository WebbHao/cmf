<?php

namespace Cmf\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Route,Gate,Auth;//Illuminate\Routing\Route;

use Cmf\Http\Requests\BasicRequest;

class HomeController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(BasicRequest $request)
    {
        return view('home');
    }

    public function routeList()
    {
        //$router = new Router();
        $list = Route::getRoutes();
        foreach($list as $key => $val){
            //echo $val->getIterator();
            echo $val->domain();
            echo implode('|', $val->methods());
            //'uri'    => $route->uri(),
            echo $val->getName();
            echo $val->getActionName();
            echo $val->getParam();
            echo '<br />';
        }
        exit();
        return (array)Route::getRoutes()->getByAction();
    }
}




        
   