<?php

namespace Cmf\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Gate,Log,Route,Auth;

use Cmf\Http\Requests\Request;

class BasicRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {   
        //return true;
        if(Auth::user()->is_supper) return true;
        $ability = Route::currentRouteAction(); 
        $ability = str_replace('Cmf\Http\Controllers\\','',$ability);
        return Gate::allows($ability);
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
