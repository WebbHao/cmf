<?php

namespace Cmf\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Gate,Log,Route,Auth;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {   
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {

        return $this->all();
    }
}
