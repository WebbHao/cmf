<?php

namespace Cmf\Http\Requests\Common;

//use Illuminate\Foundation\Http\FormRequest;
use Cmf\Http\Requests\BasicRequest;

class PermissionRequest extends BasicRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
