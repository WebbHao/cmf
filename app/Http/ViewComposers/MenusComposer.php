<?php 
namespace Cmf\Http\ViewComposers;

//use Cmf\Http\Repositories\MenuRepository;
use Cmf\Model\System\Permission;
use Illuminate\View\View;

class MenusComposer
{
    public function __construct(Permission $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }
 
    public function compose(View $view)
    {
        // $menus = $this->menuRepository->getAll()->reject(function ($mens) {
        //     return $menu->posts_count == 0;
        // });
        $menus = $this->menuRepository->getPermissionList(0,1);
        $view->with('menus', $menus);
    }
}