<?php
namespace Cmf\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class ComposerServiceProvider extends ServiceProvider
{
 
    public function boot()
    {
        View::composer('Widgets.Menus.Composer', 'Cmf\Http\ViewComposers\MenusComposer');
    }
 
    public function register(){}
}