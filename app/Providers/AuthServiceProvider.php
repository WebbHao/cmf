<?php

namespace Cmf\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Gate,Log,Auth,Route;
use Laravel\Passport\Passport;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Cmf\Model' => 'Cmf\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @author Justin.W<justin.bj@msn.com>
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        $permissions = \Cmf\Model\System\Permission::with('roles')->get();
        
        if($permissions)
        {
            foreach ($permissions as $key => $permission) {
                if($permission->control && $permission->action){
                    if($permission->params){
                        $ability = $permission->control.'@'.$permission->action.'|'.$permission->params;
                    }else{
                        $ability = $permission->control.'@'.$permission->action;
                    }
                    //Log::info($ability);
                    Gate::define($ability, function($user) use ($permission) {
                        Log::info('permission');
                        Log::info($permission);
                        Log::info($user->hasPermission($permission));
                        if(!!$user->is_supper){
                            return true;
                        }else{
                            return $user->hasPermission($permission);
                        }
                        
                    });
                }
            }
        }
        //passport api验证路由暂时先不启用
        //Passport::routes();
    }
}
