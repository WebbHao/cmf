<?php 

namespace Cmf\Contracts;

/**
 * @todo  定义模型流水线操作必须要实现的方法
 * 
 * @author  Justin.W<justin.bj@msn.com>
 * @date 2016-09-02
 * 
 */
interface IBasic
{
	/**
	 * @todo    获取模型基础配置
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	public static function getConfig();

	/**
	 * @todo    获取数据更新验证规则
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	public function getRule();

	/**
	 * @todo    获取验证结果提示信息
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	//public function getMessage();

	/**
	 * @todo    执行验证操作
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	//public function validation();

	/**
	 * @todo   处理数据
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return [type] [description]
	 */
	//public function process($model,$type='update');
}