<?php

namespace Cmf\Model\Region;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [];

    public static $_config = [
        'title'             => '国家信息维护',
        'keywords'          => '国家信息维护',
        'description'       => '国家基础信息管理',
        'router'            => '/region/country',          //路由
        'controller'        => 'Region\CountryController', //控制器
        'filter'            => true,                        //列表页是否开启条件搜索
        'pageSize'          => 20,                           //页面长度
        'orderBy'           => 'updated_at',                 //排序字段
        'orderMethod'       => 'desc',                       //排序方式
        //'templateIndex'     => 'Region.Continent.GetIndex',  //列表页模板
        //'templateEdit'      => 'Region.Continent.GetEdit',   //编辑、新建页模板
        //'templateShow'      => 'Common.Basic.GetShow',       //展示页模板
        'items'             => [
            'name_cn' => [
                'title'     => '中文名称',
                'filter'    => false,
                'type'      => 'text',
                'validator' => 'required'
            ],
            'name_en'    => [
                'title'  => '英文名称',
                'filter' => true,
                'type'   => 'text',
            ],
            'pin_yin'    => [            //字段名称
                'title'  => '汉语拼音',  //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'text',      //编辑页控件类型
                'hidden' => true,       //列表页是否显示
            ],
            'short'    => [              //字段名称
                'title'  => '简拼',      //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'text',      //编辑页控件类型
                'hidden' => true,       //列表页是否显示
            ],
            'first_letter'    => [       //字段名称
                'title'  => '首字母',    //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'text',      //编辑页控件类型
                'hidden' => true,       //列表页是否显示
            ],
            'country_code'    => [
                'title'  => '二字码',
                'filter' => true,
                'type'   => 'text',
                'hidden' => false,
            ],
            'three_code'    => [
                'title'  => '三字码',
                'filter' => false,
                'type'   => 'text',
                'hidden' => false,
            ],
            'national_flag'    => [       //字段名称
                'title'  => '国旗',    //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'image',      //编辑页控件类型
                'hidden' => false,       //列表页是否显示
            ],
            'description'    => [
                'title'  => '描述',
                'filter' => false,
                'type'   => 'textarea',
                'hidden' => true,
            ],
            'is_hot'    => [
                'title'  => '热门',
                'filter' => true,
                'type'   => 'radio',
                'hidden' => false,
                'selectItems' => [
                    '1'  => '是',
                    '0'  => '否',
                ],
            ],
            'status'    => [             //字段名称
                'title'  => '状态',      //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'radio',     //编辑页控件类型
                'hidden' => false,       //列表页是否显示
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],
        ],
    ];

    /**
     * @todo    获取模型基础配置
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public static function getConfig()
    {
        return static::$_config;
    }

    /**
     * @todo    获取数据更新验证规则
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getRule()
    {
        $config = static::getConfig();
        $rules = [];
        foreach ($config['items'] as $key => $item) {
            if (isset($item['validator'])) {
                $rules[$key] = $item['validator'];
            }
        }

        return $rules;
    }
}
