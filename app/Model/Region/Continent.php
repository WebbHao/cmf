<?php

namespace Cmf\Model\Region;

use Illuminate\Database\Eloquent\Model;

use Cmf\Contracts\IBasic;

class Continent extends Model implements IBasic
{
    protected $fillable = [];

    public static $_config = [
        'title'             => '大洲信息维护',
        'keywords'          => '大洲信息维护',
        'description'       => '大洲基础信息管理',
        'router'            => '/region/continent',          //路由
        'controller'        => 'Region\ContinentController', //控制器
        'filter'            => false,                        //列表页是否开启条件搜索
        'pageSize'          => 20,                           //页面长度
        'orderBy'           => 'updated_at',                 //排序字段
        'orderMethod'       => 'desc',                       //排序方式
        'templateIndex'     => 'Region.Continent.GetIndex',  //列表页模板
        'templateEdit'      => 'Region.Continent.GetEdit',   //编辑、新建页模板
        'templateShow'      => 'Common.Basic.GetShow',       //展示页模板
        'items'             => [
            'name_cn' => [
                'title'     => '中文名称',
                'filter'    => true,
                'type'      => 'text',
                'validator' => 'required'
            ],
            'name_en'    => [
                'title'  => '英文名称',
                'filter' => true,
                'type'   => 'text',
            ],
            'pin_yin'    => [            //字段名称
                'title'  => '汉语拼音',  //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'text',      //编辑页控件类型
                'hidden' => false,       //列表页是否显示
            ],
            'short'    => [              //字段名称
                'title'  => '简拼',      //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'text',      //编辑页控件类型
                'hidden' => false,       //列表页是否显示
            ],
            'first_letter'    => [       //字段名称
                'title'  => '首字母',    //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'text',      //编辑页控件类型
                'hidden' => false,       //列表页是否显示
            ],
            'status'    => [             //字段名称
                'title'  => '状态',      //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'radio',     //编辑页控件类型
                'hidden' => false,       //列表页是否显示
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],
        ],
    ];

    /**
     * @todo    获取模型基础配置
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public static function getConfig()
    {
        return static::$_config;
    }

    /**
     * @todo    获取数据更新验证规则
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getRule()
    {
        $config = static::getConfig();
        $rules = [];
        foreach ($config['items'] as $key => $item) {
            if (isset($item['validator'])) {
                $rules[$key] = $item['validator'];
            }
        }

        return $rules;
    }
}
