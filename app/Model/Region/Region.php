<?php

namespace Cmf\Model\Region;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = [];

    public static $_config = [
        'title'             => '区域信息维护',
        'keywords'          => '区域信息维护',
        'description'       => '区域信息管理',
        'router'            => '/region/region',          //路由
        'controller'        => 'Region\RegionController', //控制器
        'filter'            => True,                        //列表页是否开启条件搜索
        'pageSize'          => 20,                           //页面长度
        'orderBy'           => 'updated_at',                 //排序字段
        'orderMethod'       => 'desc',                       //排序方式
        //'templateIndex'     => 'Region.Continent.GetIndex',  //列表页模板
        //'templateEdit'      => 'Region.Continent.GetEdit',   //编辑、新建页模板
        //'templateShow'      => 'Common.Basic.GetShow',       //展示页模板
        'items'             => [
            'name_cn' => [
                'title'     => '中文名称',
                'filter'    => true,
                'type'      => 'text',
                'validator' => 'required'
            ],
            'name_en'    => [
                'title'  => '英文名称',
                'filter' => true,
                'type'   => 'text',
            ],
            'status'    => [
                'title'  => '状态',
                'filter' => true,
                'type'   => 'radio',
                'hidden' => false,
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],
        ],
    ];

    /**
     * @todo    获取模型基础配置
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public static function getConfig()
    {
        return static::$_config;
    }

    /**
     * @todo    获取数据更新验证规则
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getRule()
    {
        $config = static::getConfig();
        $rules = [];
        foreach ($config['items'] as $key => $item) {
            if (isset($item['validator'])) {
                $rules[$key] = $item['validator'];
            }
        }

        return $rules;
    }
}
