<?php

namespace Cmf\Model\System;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Log,Auth,Cache,DB;

use Cmf\Contracts\IBasic;

class Permission extends Model implements IBasic
{
    protected $fillable = [
        'name','icon','control','action','params','match_url','parent_id','type_id','status','sort'
    ];

    public static $_config = [
        'title'             => '权限',
        'keywords'          => '权限 维护',
        'description'       => '权限信息维护',
        'router'            => '/system/permission',
        'controller'        => 'System\PermissionController',
        'filter'            => true,
        'pageSize'          => 20,
        'orderBy'           => 'parent_id',
        'orderMethod'       => 'asc',
        'templateIndex'     => 'Common.Permission.GetIndex',  //列表页模板
        'templateEdit'      => 'Common.Permission.GetEdit',   //编辑、新建页模板
        'items'             => [
            'name' => [
                'title'     => '权限名称',
                'filter'    => true,
                'type'      => 'text',
                'validator' => 'required'
            ],
            'icon' => [
                'title'     => 'ICON',
                'filter'    => false,
                'type'      => 'text',
            ],
            'control'    => [
                'title'  => '类名',
                'filter' => true,
                'type'   => 'text',
            ],
            'action'    => [
                'title'  => '方法',
                'filter' => true,
                'type'   => 'text',
            ],
            'params'    => [
                'title'  => '参数',
                'filter' => false,
                'type'   => 'text',
            ],
            'match_url'    => [
                'title'  => '匹配路由正则',
                'filter' => false,
                'type'   => 'text',
            ],
            'parent_id'  => [
                'title'  => '父级权限',
                'filter' => true,
                'type'   => 'select',
                'hidden' => false,    //列表页是否显示
                'selectItems' => [

                ],
            ] ,
            'type_id'    => [
                'title'  => '权限类型',
                'filter' => false,
                'type'   => 'radio',
                'hidden' => false,    //列表页是否显示
                'selectItems' => [
                    '1'  => '菜单',
                    '2'  => '页面',
                    '3'  => '页面元素',
                ],
            ],
            'description'    => [
                'title'  => '角色描述',
                'filter' => false,
                'type'   => 'textarea',
                'hidden' => true,
            ],
            'sort'    => [
                'title'  => '排序',
                'filter' => false,
                'type'   => 'text',
                'hidden' => true,
            ],
            'status'    => [          //字段名称
                'title'  => '状态',   //字段描述
                'filter' => false,     //是否做为列表页的筛选条件
                'type'   => 'radio',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],
        ],
    ];

    /**
     * @todo 获取权限列表，按照级别递归结果
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  string $grade 父级id
     * @return mixed
     */
    public static function getPermissionMenuList($grade=0)
    {
        $list = self::getPermissionList($grade);
        $menuList = [];
        self::formatPermission($list,$menuList,'');
        return $menuList;
    }

    /**
     * @todo  格式化权限列表
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  array   $list      权限集合
     * @param  array   $menuList  值传递，格式化结果
     * @param  string  $prefix    权限名称前缀
     * @return mixed
     */
    public static function formatPermission($list,&$menuList=[],$prefix='')
    {
        if(is_array($list)&&count($list)){
            foreach($list as $key => $val){
                if(array_get($val,'parent_id')===0) $prefix = '';
                $menuList[$val['id']] = $prefix.$val['name'];
                $subPrefix = $prefix;
                if(array_has($val,'sub_list')&&count($val['sub_list'])){
                    if(empty($subPrefix)){
                        $subPrefix = '|---';
                    }elseif($subPrefix) $subPrefix .= '---';
                    self::formatPermission($val['sub_list'],$menuList,$subPrefix);
                }
            }
        }
        return ;
    }

    /**
     * @todo 获取权限列表，从缓存中读取，如果没有则从数据库读取
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  string $grade 父级id
     * @param  string $flag  标示，1返回菜单类型权限，0 返回全部权限
     * @return mixed
     */
    public static function getPermissionList($grade = 0,$flag = '0'){
        $list = [];
        if(Auth::check()){
            $userID = Auth::user()->id;
            $key   = 'user-permission-'.$userID.'-'.$grade.'-'.$flag;
            if(Cache::has($key)){
                $tmp  = Cache::get($key);
                $list = json_decode($list,true);
            }else{
                $list = self::getList($grade,$flag);
            }
        }
        return $list;
    }

    /**
     * @todo 获取权限列表，按照级别递归结果
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  string $grade 父级id
     * @param  string $flag  标示，1返回菜单类型权限，0 返回全部权限
     * @return mixed
     */
    public static function getList($grade=0,$flag = '0')
    {
        $recursionList = [];
        $roleIDs = Auth::user()->roles()->pluck('id')->toArray();
        //dd($roleIDs);
        try{
            $builder = self::where(function($query) use ($flag){
                $query->where('status','1');
                if($flag){
                    $query->where('type_id',$flag);
                }        
            });
            if(Auth::user()->is_supper){
                $list = $builder->orderBy('sort')->get();
            }else{
                $permissionIDs = DB::table("permission_role")->whereIn('role_id',$roleIDs)->pluck('permission_id');
                $builder = $builder->whereIn('id',$permissionIDs);
                $list = $builder->orderBy('sort')->get();
            }
        }catch(QueryException $e){
            Log::info(__FILE__.' '.__LINE__.' ：'.$e->getMessage());
        }finally {
            if(!isset($list))
                $list = [];
        }
        if($list)
        {
            $list = $list->toArray();
            recursion($list,$recursionList,$grade);
        }
        return $recursionList;
    }

    public function roles()
	{
	    return $this->belongsToMany(Role::class);
	}
	 
    /**
     * @todo    获取模型基础配置
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public static function getConfig()
    {
        $config = static::$_config;
        $permissionList = self::getPermissionMenuList();
        $config['items']['parent_id']['selectItems'] = $permissionList;
        return $config;
    }

    /**
     * @todo    获取数据更新验证规则
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getRule()
    {
        $config = static::getConfig();
        $rules = [];
        foreach ($config['items'] as $key => $item) {
            if (isset($item['validator'])) {
                $rules[$key] = $item['validator'];
            }
        }

        return $rules;
    } 
}
