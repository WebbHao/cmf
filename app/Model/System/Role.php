<?php

namespace Cmf\Model\System;

use Illuminate\Database\Eloquent\Model;

use Cmf\Contracts\IBasic;

class Role extends Model implements IBasic
{
	protected $fillable = [];

	public static $_config = [
		'title'             => '角色',
		'keywords'          => '角色 维护',
		'description'       => '用于对权限分组、便于对用户分发权限',
		'router'            => '/system/role',                  //路由
		'controller'        => 'System\RoleController', //控制器
		'filter'            => true,                     //列表页是否开启条件搜索
		'pageSize'          => 20,                       //页面长度
		'orderBy'           => 'id',                     //排序字段
		'orderMethod'       => 'desc',                   //排序方式
		'templateIndex'     => 'Common.Role.GetIndex',   //列表页模板
		'templateEdit'      => 'Common.Basic.GetEdit',   //编辑、新建页模板
		'templateShow'      => 'Common.Role.GetShow',   //展示页模板
		'items'             => [
			'name' => [
				'title'     => '角色名称',
				'filter'    => true,
				'type'      => 'text',
				'validator' => 'required'
			],
			'label'    => [
				'title'  => '角色标识',
				'filter' => true,
				'type'   => 'text',
			],
            'description'    => [        //字段名称
				'title'  => '角色描述',  //字段描述
				'filter' => true,        //是否做为列表页的筛选条件
				'type'   => 'textarea',  //编辑页控件类型
				'hidden' => false,       //列表页是否显示
            ],
            'status'    => [          //字段名称
                'title'  => '状态',   //字段描述
                'filter' => true,     //是否做为列表页的筛选条件
                'type'   => 'radio',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],
		],
	];

	protected static $_rules = [
		'name'  => 'required',
		'label' => 'required',
	];	

    public function permissions()
	{
	    return $this->belongsToMany(Permission::class);
	}

	//给角色添加权限
	public function givePermissionTo($permission)
	{
	    return $this->permissions()->save($permission);
	}

	/**
	 * @todo    获取模型基础配置
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	public static function getConfig()
	{
		return static::$_config;
	}

	/**
	 * @todo    获取数据更新验证规则
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	public function getRule()
	{
		$config = static::getConfig();
		$rules = [];
		foreach ($config['items'] as $key => $item) {
			if (isset($item['validator'])) {
				$rules[$key] = $item['validator'];
			}
		}

		return $rules;
	}

	/**
	 * @todo    获取验证结果提示信息
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	public function getMessage()
	{

	}

	/**
	 * @todo    执行验证操作
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return  mixed
	 */
	public function validation()
	{

	}

	/**
	 * @todo   处理数据
	 * 
	 * @author  Justin.W<justin.bj@msn.com>
	 * @return [type] [description]
	 */
	public function process($model,$type='update')
	{

	}
	 
}
