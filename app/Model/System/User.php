<?php

namespace Cmf\Model\System;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\HasApiTokenstrait;
use Log;

use Cmf\Contracts\IBasic;

class User extends Authenticatable implements IBasic
{
    use Notifiable,HasApiTokens;

    public static $snakeAttributes = false;

    public static $_config = [
        'title'             => '用户',
        'keywords'          => '用户 维护',
        'description'       => '用户信息维护，设置角色、状态启用或禁用以及新建',
        'router'            => '/system/user',                  //路由
        'controller'        => 'System\UserController', //控制器
        'filter'            => true,                     //列表页是否开启条件搜索
        'pageSize'          => 20,                       //页面长度
        'orderBy'           => 'id',                     //排序字段
        'orderMethod'       => 'desc',                   //排序方式
        'templateIndex'     => 'Common.user.GetIndex',   //列表页模板
        'templateEdit'      => 'Common.Basic.GetEdit',   //编辑、新建页模板
        'templateShow'      => 'Common.user.GetShow',   //展示页模板
        'items'             => [
            'name' => [
                'title'     => '用户名',
                'filter'    => true,
                'type'      => 'text',
                'validator' => 'required'
            ],
            'email'    => [
                'title'  => 'email',
                'filter' => true,
                'type'   => 'email',
                'validator' => 'required'
            ],
            'is_supper'    => [        //字段名称
                'title'  => '超级用户',  //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'radio',  //编辑页控件类型
                'hidden' => false,       //列表页是否显示
                'selectItems' => [
                    '1' => '是',
                    '0' => '否',
                ]
            ],
            'password'    => [
                'title'  => '密码',
                'filter' => false,
                'type'   => 'password',
                'hidden' => true,
            ],
            'status'    => [          //字段名称
                'title'  => '状态',   //字段描述
                'filter' => true,     //是否做为列表页的筛选条件
                'type'   => 'radio',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],
        ],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    // 判断用户是否具有某个角色
    public function hasRole($role)
    {
        Log::error($role);
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }
        Log::error($this->roles);
        return $role->intersect($this->roles)->count();
    }

    // 判断用户是否具有某权限
    public function hasPermission($permission)
    {
        Log::info($permission->roles.'=>role');
        return $this->hasRole($permission->roles);
    }

    // 给用户分配角色
    public function assignRole($role)
    {
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    /**
     * @todo    获取模型基础配置
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public static function getConfig()
    {
        return static::$_config;
    }

    /**
     * @todo    获取数据更新验证规则
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getRule()
    {
        $config = static::getConfig();
        $rules = [];
        foreach ($config['items'] as $key => $item) {
            if (isset($item['validator'])) {
                $rules[$key] = $item['validator'];
            }
        }

        return $rules;
    }

    /**
     * @todo    获取验证结果提示信息
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getMessage()
    {

    }

    /**
     * @todo    执行验证操作
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function validation()
    {

    }

    /**
     * @todo   处理数据
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return [type] [description]
     */
    public function process($model,$type='update')
    {

    }
}
