<?php

namespace Cmf\Model;

use Illuminate\Database\Eloquent\Model;

use Cmf\Contracts\IBasic;

class RoleUser extends Model implements IBasic
{
    public $table       = 'role_user';  //表名
    public $timestamps  = false;              //禁用created_at、updated_at
    public $primaryKey  = 'role_id';          //定义主键
    protected $fillable = [];

    public static $_config = [
        'title'             => '用户&角色关联',
        'keywords'          => '用户 角色 权限 关联 维护',
        'description'       => '用于对用户设置权限',
        'router'            => '/common/user-role',                  //路由
        'controller'        => 'Common\BasicController', //控制器
        'filter'            => false,                     //列表页是否开启条件搜索
        'pageSize'          => 20,                       //页面长度
        'orderBy'           => 'id',                     //排序字段
        'orderMethod'       => 'desc',                   //排序方式
        'templateIndex'     => 'Common.Basic.GetIndex',  //列表页模板
        'templateEdit'      => 'Common.Basic.GetEdit',   //编辑、新建页模板
        'templateShow'      => 'Common.Basic.GetShow',   //展示页模板
        'items'             => [
            'role_id' => [
                'title'     => '角色名称',
                'filter'    => false,
                'type'      => 'text',
                'validator' => 'required'
            ],
            'user_id'    => [
                'title'  => '角色名称',
                'filter' => false,
                'type'   => 'text',
            ],
            
        ],
    ];

    protected static $_rules = [
        'role_id'       => 'required',
        'permission_id' => 'required',
    ];  

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @todo    获取模型基础配置
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public static function getConfig()
    {
        return static::$_config;
    }

    /**
     * @todo    获取数据更新验证规则
     * 
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getRule()
    {
        $config = static::getConfig();
        $rules = [];
        foreach ($config['items'] as $key => $item) {
            if (isset($item['validator'])) {
                $rules[$key] = $item['validator'];
            }
        }

        return $rules;
    }
}
