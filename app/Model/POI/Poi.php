<?php

namespace Cmf\Model\POI;

use Cmf\Model\Region\Continent;
use Cmf\Model\Region\Country;
use Cmf\Model\Region\City;
use Cmf\Model\Region\Region;
use Cmf\Model\Common\Type;

use Illuminate\Database\Eloquent\Model;

class Poi extends Model
{
    protected $fillable     = [];
    public static $_status  = '1';

    public static $_config = [
        'title'             => 'POI信息',
        'keywords'          => 'POI信息维护',
        'description'       => 'POI基础信息管理',
        'router'            => '/poi/poi',          //路由
        'controller'        => 'POI\PoiController', //控制器
        'filter'            => true,                        //列表页是否开启条件搜索
        'pageSize'          => 20,                           //页面长度
        'orderBy'           => 'updated_at',                 //排序字段
        'orderMethod'       => 'desc',                       //排序方式
        'templateIndex'     => 'POI.Poi.GetIndex',  //列表页模板
        'templateEdit'      => 'POI.Poi.GetEdit',   //编辑、新建页模板
        'templateShow'      => 'POI.Poi.GetShow',       //展示页模板
        'items'             => [
            'name_cn' => [
                'title'     => '中文名称',
                'filter'    => true,
                'type'      => 'text',
                'validator' => 'required'
            ],
            'name_en'    => [
                'title'  => '英文名称',
                'filter' => true,
                'type'   => 'text',
            ],
            'pin_yin'    => [
                'title'  => '汉语拼音',
                'filter' => false,
                'type'   => 'text',
                'hidden' => true,
            ],
            'continent_id'    => [
                'title'  => '大洲',  //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'select',      //编辑页控件类型
                'hidden' => true,       //列表页是否隐藏
                'selectItems' => [],
            ],
            'country_id'    => [              //字段名称
                'title'  => '国家',      //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'select',      //编辑页控件类型
                'hidden' => false,       //列表页是否隐藏
                'selectItems' => [],
            ],
            'city_id'    => [
                'title'  => '城市',
                'filter' => true,
                'type'   => 'select',
                'hidden' => false,
                'selectItems' => [],
            ],
            'region_id'    => [       //字段名称
                'title'  => '区域',    //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'select',      //编辑页控件类型
                'hidden' => true,       //列表页是否隐藏
                'selectItems' => [
                    '0' => '区域',
                ],
            ],
            'street'    => [
                'title'  => '街道',
                'filter' => false,
                'type'   => 'text',
                'hidden' => true,
            ],
            'address'    => [
                'title'  => '详细地址',
                'filter' => false,
                'type'   => 'text',
                'hidden'   => true,
            ],
            'one_type_id'    => [
                'title'  => '一级分类',
                'filter' => true,
                'type'   => 'select',
                'hidden' => true,
                'selectItems' => [
                    '0' => '默认分类',
                ],
                'validator' => 'required',
            ],
            'two_type_id'    => [
                'title'  => '二级分类',
                'filter' => false,
                'type'   => 'select',
                'hidden' => true,
                'selectItems' => [
                    '0' => '默认分类',
                ],
                'validator' => 'required',
            ],
            'recommend'    => [       //字段名称
                'title'  => '推荐',    //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'radio',      //编辑页控件类型
                'hidden' => false,       //列表页是否隐藏
                'selectItems' => ['1'=>'是','0'=>'否'],
            ],
            'tips'    => [
                'title'  => '温馨提示',
                'filter' => false,
                'type'   => 'textarea',
                'hidden' => true,
            ],
            'overview'    => [
                'title'  => '概述',
                'filter' => false,
                'type'   => 'textarea',
                'hidden' => true,
            ],
            'content'    => [
                'title'  => '正文',
                'filter' => false,
                'type'   => 'textarea',
                'hidden' => true,
            ],
            'map_url'    => [
                'title'  => '地图',
                'filter' => false,
                'type'   => 'text',
                'hidden' => true,
            ],
            'longitude'    => [
                'title'  => '纬度',
                'filter' => false,
                'type'   => 'text',
                'hidden' => true,
            ],
            'latitude'    => [
                'title'  => '经度',
                'filter' => false,
                'type'   => 'text',
                'hidden' => true,
            ],
            'pv'    => [
                'title'  => 'PV',
                'filter' => false,
                'type'   => 'text',
                'hidden' => true,
                'validator' => 'required',
            ],
            'is_hot'    => [             //字段名称
                'title'  => '是否热门',      //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'radio',     //编辑页控件类型
                'hidden' => false,       //列表页是否隐藏
                'selectItems' => [
                    '1'  => '是',
                    '0'  => '否',
                ],
            ],
            'status'    => [             //字段名称
                'title'  => '状态',      //字段描述
                'filter' => true,        //是否做为列表页的筛选条件
                'type'   => 'radio',     //编辑页控件类型
                'hidden' => false,       //列表页是否隐藏
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],

        ],
    ];

    /**
     * @todo    获取模型基础配置
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public static function getConfig()
    {
        $config     = static::$_config;
        $continents = Continent::where('status',static::$_status)->pluck('name_cn','id');
        $config['items']['continent_id']['selectItems'] = $continents;
        return $config;
    }

    /**
     * @todo    获取数据更新验证规则
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getRule()
    {
        $config = static::getConfig();
        $rules = [];
        foreach ($config['items'] as $key => $item) {
            if (isset($item['validator'])) {
                $rules[$key] = $item['validator'];
            }
        }

        return $rules;
    }

    /**
     * @todo 设置大洲ID映射
     *
     * @author Justin.BJ@msn.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function continent()
    {
        return $this->belongsTo(Continent::class);
    }
    /**
     * @todo   设置国家ID关联
     *
     * @author Justin.Bj@msn.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @todo   设置城市ID关联
     *
     * @author Justin.BJ@msn.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @todo   设置区域ID关联
     *
     * @author Justin.BJ@msn.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * @todo   设置一级ID关联
     *
     * @author Justin.BJ@msn.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function oneType()
    {
        return $this->belongsTo(City::class,'one_type_id');
    }

    /**
     * @todo   设置二级ID关联
     *
     * @author Justin.BJ@msn.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function twoType()
    {
        return $this->belongsTo(City::class,'two_type_id');
    }
}
