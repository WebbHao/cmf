<?php

namespace Cmf\Console\Commands;

use Illuminate\Console\Command;
use Log,Route;

use Cmf\Model\System\Permission;

class RouteList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generatePermission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '自动生成权限';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('路由列表');
        $list = Route::getRoutes();
        foreach($list as $key => $val){
            $actionName = $val->getActionName();
            $action     = substr($actionName,21); //21 是controller包名的长度
            $urlName    = $val->getName();
            if($action && $urlName){
                list($control,$action) = explode('@',$action);
                $urlNameList           = explode('.',$urlName);
                $match_url             = current($urlNameList);
                if($match_url!='debugbar'&&$match_url!='login'&&$match_url!='logout'&&$match_url!='register'){
                    $permissionOBJ = Permission::where('control',$control)->where('action',$action);
                    if($control==='Common\BasicController'){
                        $permissionOBJ = $permissionOBJ->where('match_url',$match_url);
                    }
                    $permission = $permissionOBJ->first();
                    $msg        = '第'.($key+1).'条路由';
                    if(!$permission){
                        $arr = [
                            'name'      => $actionName,
                            'control'   => $control,
                            'action'    => $action,
                            'match_url' => $match_url,
                            'parent_id' => '0',
                            'type_id'   => '3',
                            'status'    => '0',
                        ];
                        //$this->info(json_encode($arr));//continue ;
                        if(Permission::create($arr)){
                            $this->info($msg.$urlName.'插入成功');
                        }else{
                            $this->error($msg.$urlName.'插入失败');
                        }
                    }else{
                         $this->info($msg.$urlName.'已存在');
                    }
                }
            }
        }
        $this->info('路由更新完成');
    }
}
