<?php

namespace Cmf\Console\Commands;

use Illuminate\Console\Command;
use DB;

use Cmf\Model\POI\Poi as PoiModel;

class Poi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:poi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '导入POI数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pageSize = 100;
        $this->info('poi import start......');
        $poiCount = DB::connection('sqlsrv')->table('Web_poi')->count();
        $pageCount = ceil($poiCount/$pageSize);
        $this->info('Total Page:'.$pageCount);
        foreach($this->xrange(0,$pageCount) as $key => $val){
            $this->info('processing '.$val.' page ......');
            $list = DB::connection('sqlsrv')->table('Web_poi')->select("*")->offset($val*$pageSize)->limit($pageSize)->get();
            $mList = [];
            foreach($list as $k => $v){
                //if($v->ID)
                    //$mList[$k]['id']        = $v->ID;
                $mList[$k]['name_cn']       = $v->NameCN ?:'';
                $mList[$k]['name_en']       = $v->NameEN?:'';
                $mList[$k]['pin_yin']       = $v->PinYin?:'';
                $mList[$k]['continent_id']  = $v->ContinentID?:'0';
                $mList[$k]['country_id']    = $v->CountryID?:'0';
                $mList[$k]['region_id']     = '0';//$v->RegionID?:'0';
                $mList[$k]['city_id']       = $v->CityID?:'0';
                $mList[$k]['one_type_id']   = $v->MainType?:'0';//
                $mList[$k]['two_type_id']   = $v->SubType?:'0';//
                $mList[$k]['map_url']       = $v->MapImageuRL?:'';//
                $mList[$k]['street']        = $v->MapImageuRL?:'';//
                $mList[$k]['address']       = $v->address?:'';//
                $mList[$k]['content']       = $v->Description?:'';
                $mList[$k]['overview']      = $v->overview?:'';//
                $mList[$k]['tips']          = $v->tips?:'';//
                $mList[$k]['longitude']     = $v->Lng?:'';
                $mList[$k]['latitude']      = $v->Lat?:'';
            }
            PoiModel::insert($mList);
            $this->info('processing '.$val.' page complete!');
        }
    }

    /**
     * @todo  迭代器
     *
     * @author Justin.BJ@msn.com
     * @param $start
     * @param $limit
     * @param string $step
     * @return \Generator
     */
    protected function xrange($start,$limit,$step='1'){
        for($i=$start;$i<=$limit;$i+=$step){
            yield $i;
        }
    }
}
